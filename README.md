# Triple Crown

A site to document Triple Crown online.

## Instructions

1. Clone the repo
2. Run `yarn` or `npm install`
3. Run `yarn dev` or `npm run dev`

## Documentation

This site is built on [VuePress](https://vuepress.vuejs.org/). Please see their [Guide](https://vuepress.vuejs.org/guide/) for more information on how it works.
