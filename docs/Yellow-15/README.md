---
title: Dust—All’s Yellow
meta:
    - name: description
      content: Triple Crown - Dust—All’s Yellow
    - name: keywords
      content: Triple Crown - Dust—All’s Yellow
---

# Dust—All’s Yellow

![](./media/image41.png)

When time and separation cease to be

A stranglehold unstinting and severe

Athwart the very breath that breathes in me,

The timeless far is now so very near.

And poverty no longer makes me poor,

And loneliness no longer all alone.

The pain, unchanged, persistent, without cure,

**N**o longer makes me suffer, though I groan.

Omnipotent You are, as I have heard,

But now my eyes have seen You, and your speech

Grandiloquent to me excels Your word.

If in this yellow dust the sun would bleach

My bones, I\'m still content, repaired the breach,

<p><router-link to="/yellow-2">Desire itself no longer within reach.</router-link><sup><a href="#_1">1</a></sup></p>

### Footnote

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        One who has all he wants cannot desire.
    </p>
</div>

<br>

<img class="end-image" src="/logo.png" />

<div class="prev-next">
  <router-link class="yellow" to="/yellow-14">Previous Poem</router-link>
  <router-link to="/about">About The Author</router-link>
</div>

<br>

<AmazonLink />