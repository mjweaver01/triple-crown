---
title: God’s Blue Throne
meta:
    - name: description
      content: Triple Crown - God’s Blue Throne
    - name: keywords
      content: Triple Crown - God’s Blue Throne
---

# God’s Blue Throne

![](./media/image13.png)

Roaring beasts on land, with speech unblessed,

Exemplify in awe their Maker's might,

While humble creatures likewise manifest

The attributes that honor the upright.<sup>[1](#_1)</sup>

In modesty the cat heeds nature's call.

The ant will not purloin his fellow's grain.

In chastity the dove instructs us all

**I**n how to mate for life, and so remain.

The rooster, in his wooing of the hen,

Reveals the innate wisdom that he knows—

Within a turquoise palace or a pen—

That every cock should coo before he crows.

God\'s providence extends, like His decrees,

<p><router-link to="/blue-13">Over all His creatures, land and seas.</router-link></p>

### Footnote

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        See Talmud Bavli <i>Eruvin</i> 100b. "Had not the Torah been given, we
        would have learned modesty from a cat, not to commit theft from an
        ant, not to commit adultery from a dove, and the proper manner of
        conduct for marital relations from a rooster, which first appeases
        its mate and then has relations with it."
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="blue" to="/blue-11">Previous Poem</router-link>
  <router-link class="blue" to="/blue-13">Next Poem</router-link>
</div>

<br>

<AmazonLink />