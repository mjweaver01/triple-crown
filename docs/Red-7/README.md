---
title: Oh Zealots So Red
meta:
    - name: description
      content: Triple Crown - Oh Zealots So Red
    - name: keywords
      content: Triple Crown - Oh Zealots So Red
---

# Oh Zealots So Red

![](./media/image8.png)

"Let there be light," He said, and there was light,<sup>[1](#_1)</sup>

But you enticed me and I went astray<sup>[2](#_2)</sup>

Only once, this dark, this moonless night.

You snuffed my lamp; your shadow veiled my sight,

And so unseen I could not see the way

"Let there be light," He said, and there was light.

Your ribbons wrapped about me, lacing tight;<sup>[3](#_3)</sup>

**I**nto the depths I sank, could barely pray

Only once, this dark, this moonless night.

God answered me expansively and bright,<sup>[4](#_4)</sup>

In brilliance well beyond the squint of day.

"Let there be light," He said, and there was light.

Reflecting, newly chastened and contrite,

My backward glance may briefly me betray,

Only once, this dark, this moonless night.

Though I, in twilight, penances recite,<sup>[5](#_5)</sup>

The image of that night does not decay:

"Let there be light," He said, and there was light

<p><router-link to="/red-8">Only once, this dark, this moonless night.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        David addresses Bathsheba.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See II Samuel 11:1-5.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        See Jonah 2:6-7.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        See Psalms 118:5.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        See Psalms 51:5.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="red" to="/red-6">Previous Poem</router-link>
  <router-link class="red" to="/red-8">Next Poem</router-link>
</div>

<br>

<AmazonLink />