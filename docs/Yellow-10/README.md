---
title: Dust—All’s Yellow
meta:
    - name: description
      content: Triple Crown - Dust—All’s Yellow
    - name: keywords
      content: Triple Crown - Dust—All’s Yellow
---

# Dust—All’s Yellow

![](./media/image36.png)

"You may destroy, completely extirpate,"

Replies the second friend, "That final spark

Still glowing in your soul, ascribe to fate

The recompense of man, and in your dark

Cascade deny the light.<sup>[1](#_1)</sup>  This you may do 

Oblivious to truth.  The blind deny

The light while staring at the sun—and you

**H**owl 'Justice!' just as justice you defy.

Yet consequence is plain to see for all,

Unscrolled in time, writ large and fair, condign.

Your scroll, though now a smudged and angry scrawl,

Could be a palimpsest of your design.

Your plaint is common, shameful, and untrue.

<p><router-link to="/yellow-11">Erase the yellowed parchment, start anew."</router-link></p>

### Footnote

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        See Job 8. Job's second comforter, Bildad, argues that there is
        in fact justice in the world, implying that Job may not be as
        innocent as he claims.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="yellow" to="/yellow-9">Previous Poem</router-link>
  <router-link class="yellow" to="/yellow-11">Next Poem</router-link>
</div>

<br>

<AmazonLink />