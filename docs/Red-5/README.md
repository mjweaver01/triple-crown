---
title: Oh Zealots So Red
meta:
    - name: description
      content: Triple Crown - Oh Zealots So Red
    - name: keywords
      content: Triple Crown - Oh Zealots So Red
---

# Oh Zealots So Red

![](./media/image10.png)

Evoked by love, the waves caress the beach,<sup>[1](#_1)</sup>

Erasing from the sand all mortal stain

Of struggle and the bleedings that will reach

It, washed in time by river and by rain.

And that will be our end, to perish here

Among these purple irises in bloom,<sup>[2](#_2)</sup>

With blood-stained grass to serve us as a bier,

**A**nd flowers crushed to be our only tomb.<sup>[3](#_3)</sup>

And I shall yield my kingdom and my crown—

Yes, I who lost them for a tattered hem—<sup>[4](#_4)</sup>

And like these iris petals lay me down

For all to pluck, impaled upon a stem,<sup>[5](#_5)</sup>

As thirsty swords our waning life-force leech,

<p><router-link to="/red-6">As paradise regained swells into reach.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        Saul addresses his armor-bearer.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        Mount Gilboa, where Saul died, is the home of a purple Iris
        called the Gilboa Iris.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        Only Saul's bones, not his whole body, were buried. See I Samuel
        31:12-13.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        See I Samuel 15:27.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        See I Samuel 31:4.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="red" to="/red-4">Previous Poem</router-link>
  <router-link class="red" to="/red-6">Next Poem</router-link>
</div>

<br>

<AmazonLink />