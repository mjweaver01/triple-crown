---
title: Oh Zealots So Red
meta:
    - name: description
      content: Triple Crown - Oh Zealots So Red
    - name: keywords
      content: Triple Crown - Oh Zealots So Red
---

# Oh Zealots So Red

![](./media/image28.png)

Eternally, forever, and always—<sup>[1](#_1)</sup>

And no thing new is seen beneath the sun.

The endless shuffling march of nights and days:

What was, will be; what will be has been done.<sup>[2](#_2)</sup>

You gave me wisdom, Lord, as I had sought,

And made me rich in flocks and oil and grain.

To what avail?  For all of this is naught,

**N**or ever will be more, for all is vain.<sup>[3](#_3)</sup>

The wise man and the fool have the same end.<sup>[4](#_4)</sup>

All laughter, wailing, bleeding likewise cease.

What misers horde, another man will spend,<sup>[5](#_5)</sup>

And even in the grave is doubtful peace.<sup>[6](#_6)</sup>

Before your pow'r to nullify I bow.<sup>[7](#_7)</sup>

<p><router-link to="/red-15">Delay no moment longer.  Take me now!</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>137</sup>: 
        Solomon addresses God.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>138</sup>: 
        See Ecclesiastes 1:9.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>139</sup>: 
        See Ecclesiastes 2:1-11.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        See Ecclesiastes 2:14.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        See Ecclesiastes 6:1-2.
    </p>
</div>

<div class="footnote">
    <p id="_6">
        <sup>6</sup>: 
        See Ecclesiastes 3:19-21.
    </p>
</div>

<div class="footnote">
    <p id="_7">
        <sup>7</sup>: 
        See Ecclesiastes 4:2.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="red" to="/red-13">Previous Poem</router-link>
  <router-link class="red" to="/red-15">Next Poem</router-link>
</div>

<br>

<AmazonLink />