---
title: About The Author
pageClass: about
meta:
    - name: description
      content: Triple Crown - About The Author
    - name: keywords
      content: Triple Crown - About The Author
---

<div class="about-flex">
  <img src="./media/drc.png" />
  <div>
    <h1>About The Author</h1>
    <p>
      Eric Chevlen, MD, was born in Youngstown, OH, in 1949. Before writing
      <i>Triple Crown</i>, he practiced medical oncology and pain medicine. Thirty
      years' experience in the care of desperately ill and suffering people, and
      decades of study of Holy Scripture culminated in his writing this book.
    </p>
    <AmazonLink />
  </div>
</div>

