const path = require('path')

module.exports = {
  title: 'Triple Crown',
  head: [
		['link', { rel: 'icon', href: `/favicon.png` }]
	],
  description: 'Triple Crown is the only triple heroic crown of sonnets in the English language. The highly structured and interwoven poems deal with three aspects of reality as seen through the lens of scripture: the universal, the societal, and the personal. The first cycle of sonnets has a universal theme, the retelling of the story of Creation. The second cycle describes the conflicts of the kings of ancient Israel in the early days of its national existence. In the final cycle, Job describes his suffering, and rails against a world which appears to be devoid of justice.',
  themeConfig: {
    logo: '/logo.png',
    nav: [
      {
        text: 'Introduction',
        link: '/introduction/'
      },
      {
        text: 'God’s Blue Throne',
        link: '/blue-1/'
      },
      {
        text: 'Oh Zealots So Red',
        link: '/red-1/'
      },
      {
        text: 'Dust—All’s Yellow',
        link: '/yellow-1/'
      },
      {
        text: 'About The Author',
        link: '/about/'
      },
      {
        text: 'Contact The Author',
        link: '/contact/'
      }
    ],
    plugins: [
      '@vuepress/active-header-links',
    ],
    search: false,
  },
  plugins: [
    [
      '@vuepress/google-analytics',
      {
        'ga': 'UA-162768038-2'
      }
    ],
    [
      'sitemap', {
        hostname: 'https://triplecrownpoetry.com'
      }
    ]
  ],
  clientRootMixin: path.resolve(__dirname, 'scrollMixin.js')
}