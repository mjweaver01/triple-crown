export default {
    updated() {
        function scrollTo(element) {
            const scrollElement = element.closest('p')
            const y = scrollElement.getBoundingClientRect().top - (scrollElement.getBoundingClientRect().height * 3) + window.pageYOffset
            window.scroll({
                behavior: 'smooth',
                left: 0,
                top: y
            });

            scrollElement.classList.add('highlight')
            setTimeout(() => {
                scrollElement.classList.remove('highlight')
            }, 2000)
        }

        //scroll links
        var scrollLinks = document.querySelectorAll('.page p a[href*="#_"]')
        scrollLinks.forEach(function(element) {
            element.addEventListener('click', (e) => {
                e.preventDefault();
                scrollTo(document.querySelector(e.target.getAttribute('href')))
            });
        });

        //reverse scroll links
        var scrollLinks = document.querySelectorAll('.page p[id*="_"]')
        scrollLinks.forEach(function(element) {
            element.addEventListener('click', (e) => {
                e.preventDefault();
                scrollTo(document.querySelector('.page p a[href*="#' + element.id + '"]'))
            });
        });
    }
}