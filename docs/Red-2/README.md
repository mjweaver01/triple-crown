---
title: Oh Zealots So Red
meta:
    - name: description
      content: Triple Crown - Oh Zealots So Red
    - name: keywords
      content: Triple Crown - Oh Zealots So Red
---

# Oh Zealots So Red

![](./media/image18.png)

"Oh let us be united, arms outreach!"<sup>[1](#_1)</sup>

You greet me *thus*? Your greeting only mocks

*Your* broken state, resounds the wretched bleats

Of pilfered herds forbidden and the flocks<sup>[2](#_2)</sup>

Of reddled rams<sup>[3](#_3)</sup> consigned to death.  See there

Your enemy in chains.<sup>[4](#_4)</sup>  And yet, this day will bring

For *you* a broken link,<sup>[5](#_5)</sup> for *him* an heir,<sup>[6](#_6)</sup>

**N**or spare my robe,<sup>[7](#_7)</sup> your kingdom, or that king.

Oh Saul, my Saul, if only you had been

More than a donkey-chaser in your eyes,<sup>[8](#_8)</sup>

You might have lived in full what life can mean.

Now go—and live a life you must despise:

Go wallow in all corporal delight,

<p><router-link to="/red-3">Hear songs of love, and poetry recite.</router-link><sup><a href="#_9">9</a></sup></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        Saul greets Samuel after the capture of Agag. See I Samuel 15:13.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        Samuel replies to Saul with rebuke. See I Samuel 15:14.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        Reddle (sometimes called ruddle) is a powder made of red ochre.
        It is used to mark sheep. Cf. the extensive use of reddle in <i>Return
        of the Native</i>.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        See I Samuel 15:32.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        See I Samuel 15:24.
    </p>
</div>

<div class="footnote">
    <p id="_6">
        <sup>6</sup>: 
        During the time between his capture and execution, Agag sired a
        son. See Talmud Bavli <i>Megillah</i> 13a. "...for Saul did not kill
        Agag, from whom descended Haman, who oppresses the Jews." See also
        Esther 3:1.
    </p>
</div>

<div class="footnote">
    <p id="_7">
        <sup>7</sup>: 
        See I Samuel 15:27.
    </p>
</div>

<div class="footnote">
    <p id="_8">
        <sup>8</sup>: 
        See I Samuel 15:17. See also I Samuel 9:3.
    </p>
</div>

<div class="footnote">
    <p id="_9">
        <sup>9</sup>: 
        See I Samuel 16:19-23.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="red" to="/red-1">Previous Poem</router-link>
  <router-link class="red" to="/red-3">Next Poem</router-link>
</div>

<br>

<AmazonLink />