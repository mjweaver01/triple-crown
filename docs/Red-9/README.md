---
title: Oh Zealots So Red
meta:
    - name: description
      content: Triple Crown - Oh Zealots So Red
    - name: keywords
      content: Triple Crown - Oh Zealots So Red
---

# Oh Zealots So Red

![](./media/image23.png)

The barren walls reverberate a screech—<sup>[1](#_1)</sup>

My son, my son, O Absalom, my son!<sup>[2](#_2)</sup>

But daddy's lullabies will no more reach

My son, my son, O Absalom, my son!

No more, my son, I'll peel the prickly pear<sup>[3](#_3)</sup>

For you, no more embrace you while my fingers

Plow love furrows through your crown of hair.<sup>[4](#_4)</sup>

**G**one is hope, my son,<sup>[5](#_5)</sup> though yearning lingers.

Gone too those chilly nights when you would climb

Into my bed, and curl beside me, gently snore,

And warm yourself and me at the same time.

Such nights of warmth and whispers—nevermore!

Reverberant, this night, with words unsaid;

<p><router-link to="/red-10">So cold a night—and none to warm my bed.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>105</sup>: 
        David addresses the dead Absalom.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See II Samuel 19:1-5.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>:
        In Israel, the prickly pear is known as the <i>sabra</i>. Native-born
        Israelis have adopted the name for themselves, since the fruit is
        prickly on the outside, but sweet on the inside. The sweet pulp of
        the fruit is orange color.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        Absalom had remarkably abundant hair. See II Samuel 14:26.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        According to Talmud Bavli <i>Sotah</i> 10b, David uttered the word(s)
        "my son" eight times—seven times to raise Absalom from the seven
        chambers of Gehinnom (the underworld), and one more time to elevate
        him to the World to Come.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="red" to="/red-8">Previous Poem</router-link>
  <router-link class="red" to="/red-10">Next Poem</router-link>
</div>

<br>

<AmazonLink />