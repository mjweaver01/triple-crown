---
title: Dust—All’s Yellow
meta:
    - name: description
      content: Triple Crown - Dust—All’s Yellow
    - name: keywords
      content: Triple Crown - Dust—All’s Yellow
---

# Dust—All’s Yellow

![](./media/image30.png)

<p><router-link to="/yellow-2"><strong>D</strong>esire itself no longer within reach,</router-link></p>

<p><router-link to="/yellow-3"><strong>U</strong>nstinting and severe, the hands that smite—</router-link></p>

<p><router-link to="/yellow-4"><strong>S</strong>uch justice, and its judge, do I indict.</router-link></p>

<p><router-link to="/yellow-5"><strong>T</strong>he barren walls reverberate a screech</router-link></p>

<p><router-link to="/yellow-6"><strong>A</strong>s muzzy-mumble sages nod and teach</router-link></p>

<p><router-link to="/yellow-7">"<strong>L</strong>et there be light," He said, and there was light.</router-link></p>

<p><router-link to="/yellow-8"><strong>L</strong>ike scattered stars abandoned to the night,</router-link></p>

<p><router-link to="/yellow-9"><strong>S</strong>afeguard them nigh—and me within your reach</router-link></p>

<p><router-link to="/yellow-10"><strong>Y</strong>ou may destroy, completely extirpate.</router-link></p>

<p><router-link to="/yellow-11"><strong>E</strong>rase the yellowed parchment, start anew.</router-link></p>

<p><router-link to="/yellow-12"><strong>L</strong>ament some other human tragedy,</router-link></p>

<p><router-link to="/yellow-13"><strong>L</strong>est man believe that he is thrall to fate.</router-link></p>

<p><router-link to="/yellow-14"><strong>O</strong>ne is ever one and never two,</router-link></p>

<p><router-link to="/yellow-15"><strong>W</strong>hen time and separation cease to be.</router-link></p>

<br>

<div class="prev-next">
  <router-link class="red" to="/red-15">Previous Poem</router-link>
  <router-link class="yellow" to="/yellow-2">Next Poem</router-link>
</div>

<br>

<AmazonLink />