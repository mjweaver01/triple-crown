---
title: Oh Zealots So Red
meta:
    - name: description
      content: Triple Crown - Oh Zealots So Red
    - name: keywords
      content: Triple Crown - Oh Zealots So Red
---

# Oh Zealots So Red

![](./media/image17.png)

<p><router-link to="/red-2"><strong>O</strong>h let us be united, arms outreach,</router-link></p>

<p><router-link to="/red-3"><strong>H</strong>ear songs of love, and poetry recite—</router-link></p>

<p><router-link to="/red-4"><strong>Z</strong>ither, myrrh, red wine—ev'ry delight</router-link></p>

<p><router-link to="/red-5"><strong>E</strong>voked by love.  The waves caress the beach,</router-link></p>

<p><router-link to="/red-6"><strong>A</strong>s paradise regained swells into reach.</router-link></p>

<p><router-link to="/red-7"><strong>"L</strong>et there be light," He said, and there was light...</router-link></p>

<p><router-link to="/red-8"><strong>O</strong>nly once, this dark, this moonless night</router-link></p>

<p><router-link to="/red-9"><strong>T</strong>he barren walls reverberate a screech—</router-link></p>

<p><router-link to="/red-10"><strong>S</strong>o cold a night, and none to warm my bed.</router-link></p>

<p><router-link to="/red-11"><strong>S</strong>o long a night, I scarce recall the days</router-link></p>

<p><router-link to="/red-12"><strong>O</strong>f cedar paneled chambers and the vow.</router-link></p>

<p><router-link to="/red-13"><strong>R</strong>epeat it now exactly as you said—</router-link></p>

<p><router-link to="/red-14"><strong>E</strong>ternally, forever and always!</router-link></p>

<p><router-link to="/red-15"><strong>D</strong>elay no moment longer.  Take me now!</router-link></p>

<br>

<div class="prev-next">
  <router-link class="blue" to="/blue-15">Previous Poem</router-link>
  <router-link class="red" to="/red-2">Next Poem</router-link>
</div>

<br>

<AmazonLink />