---
title: Oh Zealots So Red
meta:
    - name: description
      content: Triple Crown - Oh Zealots So Red
    - name: keywords
      content: Triple Crown - Oh Zealots So Red
---

# Oh Zealots So Red

![](./media/image27.png)

"Repeat it now exactly as you said."<sup>[1](#_1)</sup>

I told them that for you, Zabud, not me.

Which mother had the live son, which the dead,

Was clear to me when each had made her plea.<sup>[2](#_2)</sup>

But how to get my court to know that fact?<sup>[3](#_3)</sup>

I ordered that the boy be cut in two,

And both of them were certain to react

**E**xactly as they did for all to view.<sup>[4](#_4)</sup>

The wisdom of a king is what I sought,<sup>[5](#_5)</sup>

To know the reddened hand and icy heart,

And not forsake the lesson mother taught.<sup>[6](#_6)</sup>

A king, in wisdom, also plays a part.

Such burning wisdom in a king must blaze

<p><router-link to="/red-14">Eternally, forever and always.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        Solomon address his friend Zabud. See I Kings 4:5.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        The two women present their case differently. The plaintiff
        first discusses the dead child, and the defendant first discusses
        the live child. Each is describing her own child.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        Solomon knew which was the real mother before he ordered that a
        sword be brought to cut the child in half. But if he simply made a
        ruling, the court might think that the decision was arbitrary or
        incorrect.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        Solomon recognized that the two women were living together
        without a man because they were mother- and daughter-in-law, both of
        whose husbands had recently died. The daughter-in-law had a motive
        to lie, because then she would appear to be relieved of having to
        wait until the boy were 13 years old to relieve her of the duty of
        levirate marriage (<i>yibum</i>). All the more so she had a motive to see
        the child killed by the king, because then in actuality, not merely
        appearance, she would be released from a levirate marriage. It is
        ordinary insight to recognize a woman who would have the baby killed
        as not being the real mother. But Solomon knew that the false
        claimant would not simply say, "See! She says give the baby to me.
        She is willing to kidnap but not to murder!" His wisdom lay in his
        knowing that she would agree to having the baby killed, not merely
        in recognizing that her saying so identified her as the false
        claimant.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        See I Kings 3:5-9.
    </p>
</div>

<div class="footnote">
    <p id="_6">
        <sup>6</sup>: 
        See Proverbs 1:8.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="red" to="/red-12">Previous Poem</router-link>
  <router-link class="red" to="/red-14">Next Poem</router-link>
</div>

<br>

<AmazonLink />
