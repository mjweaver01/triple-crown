---
title: Dust—All’s Yellow
meta:
    - name: description
      content: Triple Crown - Dust—All’s Yellow
    - name: keywords
      content: Triple Crown - Dust—All’s Yellow
---

# Dust—All’s Yellow

![](./media/image23.png)

The barren walls reverberate a screech

Of frantic ululation, words aflame.

The barren mother,<sup>[1](#_1)</sup> desp\'rate, claws to reach

For cause, for purpose, meaning—or for blame.

\"Disease and dispossession, graceless death

Do not befall the sinless.  While I live,"

She says, "I hate you, 'til my final breath,

**E**xcoriate your name, and not forgive.

Curse God and die."<sup>[2](#_2)</sup>  She never speaks again.

We eat our lentils silently and cold,<sup>[3](#_3)</sup>

And silently and cold she knows my pain

Is more than she can bear, and she has told

The bold and dread conclusion I must reach,

<p><router-link to="/yellow-6">As muzzy-mumble sages nod and teach.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        Job's wife, too, is childless.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See Job 2:9.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        The lentils of the near middle east are orange in color. Lentils
        are a traditional meal of mourning.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="yellow" to="/yellow-4">Previous Poem</router-link>
  <router-link class="yellow" to="/yellow-6">Next Poem</router-link>
</div>

<br>

<AmazonLink />