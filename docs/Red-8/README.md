---
title: Oh Zealots So Red
meta:
    - name: description
      content: Triple Crown - Oh Zealots So Red
    - name: keywords
      content: Triple Crown - Oh Zealots So Red
---

# Oh Zealots So Red

![](./media/image22.png)

Only once, this dark, this moonless night<sup>[1](#_1)</sup>

He reached out, quite by habit—she was gone,

Who'd shared his bread, his cup, his bed.<sup>[2](#_2)</sup>  Delight

Itself the thief had grabbed. What's to be done

To such a one, my king—how do you hold?

Yes, justice will proceed just as you plan;

He merits death, must compensate fourfold.<sup>[3](#_3)</sup>

**N**ow one more thing, my king:  *You* are the man!<sup>[4](#_4)</sup>

Anointed one of God, you have forsaken

God, and pampered evil in your heart.

Now from your sundered house will four be taken,<sup>[5](#_5)</sup>

The blood-stained dripping sword will not depart.<sup>[6](#_6)</sup>

When justice to the battlements will reach,

<p><router-link to="/red-9">The barren walls reverberate a screech.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        Nathan addresses David.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See II Samuel 12:3.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        See II Samuel 12:5-6. See also Exodus 21:37.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        See II Samuel 12:7.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        See Talmud Bavli <i>Yoma</i> 12b. In fact, four were indeed taken
        from among David's offsprings: Bathsheba's first-born child, who
        died in infancy (II Samuel 2:18); Tamar, who was raped (II Samuel
        13:14); Amnon, who was murdered in revenge by his half-brother (II
        Samuel 13:29); and Absalom, who died in a war of rebellion against
        his father (II Samuel 18:14-15).
    </p>
</div>

<div class="footnote">
    <p id="_6">
        <sup>6</sup>: 
        See II Samuel 12:10.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="red" to="/red-7">Previous Poem</router-link>
  <router-link class="red" to="/red-9">Next Poem</router-link>
</div>

<br>

<AmazonLink />