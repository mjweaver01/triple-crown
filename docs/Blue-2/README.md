---
title: God’s Blue Throne
meta:
    - name: description
      content: Triple Crown - God’s Blue Throne
    - name: keywords
      content: Triple Crown - God’s Blue Throne
---

# God’s Blue Throne

![](./media/image3.png)

Grandiloquent,<sup>[1](#_1)</sup> creating with just<sup>[2](#_2)</sup> speech,

The echo of His first substantial<sup>[3](#_3)</sup> word,

Extending from creation\'s farthest reach

Suffuses our blue world, and still is heard.<sup>[4](#_4)</sup>

Its symmetry, established from the first,

Gives witness to inflation\'s breathless run,<sup>[5](#_5)</sup>

When all that is, or ever will be, burst,

**W**hen all the universe, like God, was one.

But now the echoes' waves are nearly still,

Perhaps would be the absolute of cold,<sup>[6](#_6)</sup>

If God did not observe them by His will,<sup>[7](#_7)</sup>

And constantly rejuvenate the old.

We too exist, Lord, only in Your sight,

<p><router-link to="/blue-3">O God, whom angels praise by day and night,</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        The first word of the poem is itself grandiose to suggest the
        grandeur and eloquence of the word by which the world was created.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        Throughout most of this book, the word "just" is ambiguous as to
        whether it means "rightful," "merely," or both.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        The first words—"Let there be light"—created substance.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        The background cosmic radiation is the observable remnant of the
        Big Bang.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        The observable universe is remarkably uniform on a large scale.
        Since distant parts of the universe cannot communicate within the
        time since the Big Bang, this uniformity is evidence that the
        universe expanded explosively in an inflationary period shortly
        after the Big Bang.
    </p>
</div>

<div class="footnote">
    <p id="_6">
        <sup>6</sup>: 
        The temperature of the cosmic microwave background radiation is
        less than three degrees (Celsius) above absolute zero.
    </p>
</div>

<div class="footnote">
    <p id="_7">
        <sup>7</sup>: 
        One of the tenets of quantum mechanics is that a quantum level
        event transitions from potential to actual only upon observation.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="blue" to="/blue-1">Previous Poem</router-link>
  <router-link class="blue" to="/blue-3">Next Poem</router-link>
</div>

<br>

<AmazonLink />