---
title: Dust—All’s Yellow
meta:
    - name: description
      content: Triple Crown - Dust—All’s Yellow
    - name: keywords
      content: Triple Crown - Dust—All’s Yellow
---

# Dust—All’s Yellow

![](./media/image35.png)

Like scattered stars abandoned to the night,

Too feeble the great darkness to adorn,

Now come my friends to loom athwart my light,

To triply crown my sorrow with their scorn.

"The world was not created," says the first,<sup>[1](#_1)</sup>

"To be surrendered, sundered in the maw

Of chaos.  Rather, from the primal burst

**C**ontinues on its course and by its law.

The close of day, the yellow autumn's end,

The crumbling of a wall, a kingdom's fall,

Our shabby motley fabric and its rend

Convince the fool that chaos governs all.

The wise will see this, bow in awe, beseech,

<p><router-link to="/yellow-9">'Safeguard them nigh, and me, within your reach.'"</router-link></p>

### Footnote

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        See Job 4-6. Job's first comforter, Eliphaz, argues that the
        world is justly run according to divinely established natural laws.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="yellow" to="/yellow-7">Previous Poem</router-link>
  <router-link class="yellow" to="/yellow-9">Next Poem</router-link>
</div>

<br>

<AmazonLink />