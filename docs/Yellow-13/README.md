---
title: Dust—All’s Yellow
meta:
    - name: description
      content: Triple Crown - Dust—All’s Yellow
    - name: keywords
      content: Triple Crown - Dust—All’s Yellow
---

# Dust—All’s Yellow

![](./media/image39.png)

Lest man believe that he is thrall to fate.

Or history unfurls without an aim,

I call on God.  Appear—and advocate

My claim of innocence.  Or if You blame

Me for transgressions, state Your case.<sup>[1](#_1)</sup>  Remind me

Of Your storied truth, at last reveal

Your hidden justice, tell me why You grind me

**L**ike a pollen grain beneath Your heel.

You are one, alone, unique in awe;

I am one, alone, unique in woe.

Establish justice, melding truth with law,

Not just in lofty heights, but here below.

Duplicity deny and faith renew.

<p><router-link to="/yellow-14">One is ever one and never two.</router-link></p>

### Footnote

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        See Job 13:17-23. Job continues to plead his innocence and
        demands that God respond to his testimony.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="yellow" to="/yellow-12">Previous Poem</router-link>
  <router-link class="yellow" to="/yellow-14">Next Poem</router-link>
</div>

<br>

<AmazonLink />