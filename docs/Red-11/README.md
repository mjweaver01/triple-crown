---
title: Oh Zealots So Red
meta:
    - name: description
      content: Triple Crown - Oh Zealots So Red
    - name: keywords
      content: Triple Crown - Oh Zealots So Red
---

# Oh Zealots So Red

![](./media/image25.png)

*So long a night, I scarce recall the days*<sup>[1](#_1)</sup>

*Before ambition seized my widowed heart.*

*Yes, open, lord, my lips; I'll tell your praise.*<sup>[2](#_2)</sup>

*A girl of seven years can play this part.*<sup>[3](#_3)</sup>

Remember, husband-king, the vow you made

Before we wed: "Your son will reign," you said,<sup>[4](#_4)</sup>

"When, with my fathers, I at last am laid

**F**orever in my shroud and cedar bed.\"

Now may you live forever, O my lord.

Now crown my son and seat him on your chair,

Your red-eyed wife and nation have implored.

*While still your life hangs by its brittle hair,*<sup>[5](#_5)</sup>

*Let only these two thoughts your mind allow:*

<p><router-link to="/red-12"><i>Of cedar paneled chambers,</i></router-link><sup><a href="#_6">6</a></sup><router-link to="/red-12">&nbsp;<i>and the vow.</i></router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        Bathsheba's inner thoughts are in italics.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See Psalms 51:15. Bathsheba considers the verse quite
        differently from the way David did.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        The name Bathsheba means "seven year old girl" in Hebrew.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        See I Kings 1:17.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        Brittle hair, cold intolerance and mental clouding are symptoms
        of end-stage hypothyroidism, a possible cause of David's death.
    </p>
</div>

<div class="footnote">
    <p id="_6">
        <sup>6</sup>: 
        In the Bronze Age, some royalty were buried in cedar coffins.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="red" to="/red-10">Previous Poem</router-link>
  <router-link class="red" to="/red-12">Next Poem</router-link>
</div>

<br>

<AmazonLink />