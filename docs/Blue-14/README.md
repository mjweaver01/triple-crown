---
title: God’s Blue Throne
meta:
    - name: description
      content: Triple Crown - God’s Blue Throne
    - name: keywords
      content: Triple Crown - God’s Blue Throne
---

# God’s Blue Throne

![](./media/image15.png)

Nature's lord He sets, the man and wife,

Into a paradise of their demesne,

But disobedience<sup>[1](#_1)</sup> and causeless strife<sup>[2](#_2)</sup>

Expel them to the thrall of death and pain,<sup>[3](#_3)</sup>

She, to bear her cyanotic young,

In clutch-throat anguish whisper, "Breathe, my child!"

And he, amid the brow-sweat, mud, and dung,

**D**ethroned as Eden's king, to tame the wild.

And yet\...the source of blessings nonetheless

Ordains a day when tribulations end,

A day that man or God or both may bless,

And spirit over matter yet transcend:

No more weak days<sup>[4](#_4)</sup> in grubbing futile quest;

<p><router-link to="/blue-15">Evermore and more, God grants us rest.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        An homage to the first line of Milton's <i>Paradise Lost</i>.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See Talmud Bavli <i>Yoma</i> 9b. "The Second Temple...was destroyed
        because of the gratuitous hatred that existed there."
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        See Genesis 3:23.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        "Weak days" is a homonym of "week days."
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="blue" to="/blue-13">Previous Poem</router-link>
  <router-link class="blue" to="/blue-15">Next Poem</router-link>
</div>

<br>

<AmazonLink />