---
title: God’s Blue Throne
meta:
    - name: description
      content: Triple Crown - God’s Blue Throne
    - name: keywords
      content: Triple Crown - God’s Blue Throne
---

# God’s Blue Throne

![](./media/image12.png)

He fills the azure seas with teeming life

But not His image.<sup>[1](#_1)</sup> That He would not keep

Amid the turbulence and tide, the strife

Of predator and prey within the deep.

Nor did He place His likeness in the breeze

Which blows alike for dust and cloud and hawk,

Indifferent, sowing pollen and disease.

**H**e chose the land—is He not called the Rock?—<sup>[2](#_2)</sup>

To bear His image, soil of every clime,<sup>[3](#_3)</sup>

And formed the creature there upon the block

Where He will set His house again in time.<sup>[4](#_4)</sup>

He gave him speech. His creature-child can talk

And pray, and curse. Not so the rest:

<p><router-link to="/blue-12">Roaring beasts on land, with speech unblessed.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        Since God's image is not a physical appearance, it could have
        been granted to a creature of the sea or sky.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See Psalms 18:3.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        See Rashi on Genesis 2:7. "God collected man's soil from all the
        earth, from the four directions, so that anywhere man may die, there
        the earth will take him in for burial."
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        Adam was created at the site where the Temple would be built. See
        Maimonides, <i>Mishneh Torah</i>, 
        <i>Hilchos Bais HaBechirah</i>, 2:2. "Adam,
        the first man, offered a sacrifice there and was created at that
        very spot, as our Sages said, 'Man was created from the place where
        he would find atonement.'" See also Jerusalem Talmud (hereinafter
        referred to as Yerushalmi), <i>Nazir</i> 7:2.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="blue" to="/blue-10">Previous Poem</router-link>
  <router-link class="blue" to="/blue-12">Next Poem</router-link>
</div>

<br>

<AmazonLink />