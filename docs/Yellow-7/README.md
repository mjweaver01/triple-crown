---
title: Dust—All’s Yellow
meta:
    - name: description
      content: Triple Crown - Dust—All’s Yellow
    - name: keywords
      content: Triple Crown - Dust—All’s Yellow
---

# Dust—All’s Yellow

![](./media/image8.png)

"Let there be light," He said, and there was light,

So long ago and now so far away,

Like scattered stars abandoned to the night.

The world, which once we thought could hold delight,

A wasteland now, it mocks the wasted day

"Let there be light," He said, and there was light.

And we too have been banished from His sight,

**I**nvisible, to blindly feel our way,

Like scattered stars abandoned to the night.

Nor shall we know redemption from our plight,

Nor does this hoary fact our grief allay:

"Let there be light," He said, and there was light.

Like dust indifferent wind blows from a height,

We float unseen, and slowly drift away,

Like scattered stars abandoned to the night.

How grand was the creation—and how slight

The part, if any, we are called to play.

"Let there be light," He said, and there was light,

<p><router-link to="/yellow-8">Like scattered stars abandoned to the night.</router-link></p>

<br>

<div class="prev-next">
  <router-link class="yellow" to="/yellow-6">Previous Poem</router-link>
  <router-link class="yellow" to="/yellow-8">Next Poem</router-link>
</div>

<br>

<AmazonLink />