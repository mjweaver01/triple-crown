---
title: God’s Blue Throne
meta:
    - name: description
      content: Triple Crown - God’s Blue Throne
    - name: keywords
      content: Triple Crown - God’s Blue Throne
---

# God’s Blue Throne

![](./media/image2.png)

<p><router-link to="/blue-2"><strong>G</strong>randiloquent, creating with just speech,</router-link></p>

<p><router-link to="/blue-3"><strong>O</strong> God, whom angels praise by day and night,</router-link></p>

<p><router-link to="/blue-4"><strong>D</strong>eign these crowns, too, be worthy in Your sight.</router-link></p>

<p><router-link to="/blue-5"><strong>S</strong>afeguard them nigh, and me, within Your reach.</router-link></p>

<p><router-link to="/blue-6"><strong>B</strong>efore the force of nature was unleashed,</router-link></p>

<p><router-link to="/blue-7">"<strong>L</strong>et there be light," He said, and there was light.</router-link></p>

<p><router-link to="/blue-8">(<strong>U</strong>p to us, the sparks to reunite.)</router-link></p>

<p><router-link to="/blue-9"><strong>E</strong>voked by love, the waves caress the beach.</router-link></p>

<p><router-link to="/blue-10"><strong>T</strong>he land is blanketed with grass and trees.</router-link></p>

<p><router-link to="/blue-11"><strong>H</strong>e fills the azure seas with teeming life,</router-link></p>

<p><router-link to="/blue-12"><strong>R</strong>oaring beasts on land, with speech unblessed.</router-link></p>

<p><router-link to="/blue-13"><strong>O</strong>ver all His creatures, land and seas,</router-link></p>

<p><router-link to="/blue-14"><strong>N</strong>ature's lord He sets, the man and wife.</router-link></p>

<p><router-link to="/blue-15"><strong>E</strong>vermore and more, God grants us rest.</router-link></p>

<br>

<div class="prev-next">
  <router-link to="/introduction">Introduction</router-link>
  <router-link class="blue" to="/blue-2">Next Poem</router-link>
</div>

<br>

<AmazonLink />