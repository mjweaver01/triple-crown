---
home: false
sitemap:
  exclude: false
pageClass: home
---

<header class="hero"><img src="/logo.png" alt="hero"> 
    <h1 id="main-title">Triple Crown</h1> 
    <p class="description">
        <i>Triple Crown</i> is the only triple heroic crown of sonnets in the English language. The highly structured and interwoven poems deal with three aspects of reality as seen through the lens of scripture: the universal, the societal, and the personal. The first cycle of sonnets has a universal theme, the retelling of the story of Creation. The second cycle describes the conflicts of the kings of ancient Israel in the early days of its national existence. In the final cycle, Job describes his suffering, and rails against a world which appears to be devoid of justice.
    </p> 
    <p class="action">
        <a href="/introduction/" class="nav-link action-button">Read The Introduction</a>
    </p>
    <div class="footer">
        <AmazonLink />
        <p>© 2020 Dr. Eric Chevlen</p>
    </div>
</header>