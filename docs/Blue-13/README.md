---
title: God’s Blue Throne
meta:
    - name: description
      content: Triple Crown - God’s Blue Throne
    - name: keywords
      content: Triple Crown - God’s Blue Throne
---

# God’s Blue Throne

![](./media/image14.png)

Over all His creatures, land and seas,

A pair and paradox He sets to reign,

The creatures who reach higher on their knees,<sup>[1](#_1)</sup>

Whose aggrandizement ever is in vain.<sup>[2](#_2)</sup>

To know of good and evil and its tree,

They eat its fruit, and open wide their eyes,<sup>[3](#_3)</sup>

No longer good, no longer evil see

**D**iscrete, but good-and-evil in disguise.<sup>[4](#_4)</sup>

A paragon<sup>[5](#_5)</sup> and paradox is man,

Alone allowed approach to God's blue throne,

Alone enabled to defy His plan,

Alone a soul interred in flesh and bone.

One soul placed in two bodies for their life,<sup>[6](#_6)</sup>

<p><router-link to="/blue-14">Nature's lord, He sets the man and wife.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        The Hebrew word for blessing, <i>berachah</i>, is etymologically
        related to the Hebrew word for knee, <i>barach</i>.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See Genesis 11:4-9.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        See Genesis 3:7.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        The expression "good and evil" is a merism. Before the First Sin,
        Adam and Eve could easily distinguish between good and evil. After
        it, they saw deeds as a mixture of the two.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        See <i>Hamlet</i>, Act II Scene 2. "The paragon of animals!"
    </p>
</div>

<div class="footnote">
    <p id="_6">
        <sup>6</sup>: 
        See Zohar III, 7b. "The union of male and female is termed
        'one.' ... For a male without a female is called 'half a body,'
        and a half is not 'one.' When the two halves unite, they then
        become one body, and they are then called 'one.'" See also Plato's
        <i>Symposium</i>.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="blue" to="/blue-12">Previous Poem</router-link>
  <router-link class="blue" to="/blue-14">Next Poem</router-link>
</div>

<br>

<AmazonLink />