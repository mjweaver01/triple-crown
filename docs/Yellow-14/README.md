---
title: Dust—All’s Yellow
meta:
    - name: description
      content: Triple Crown - Dust—All’s Yellow
    - name: keywords
      content: Triple Crown - Dust—All’s Yellow
---

# Dust—All’s Yellow

![](./media/image40.png)

*"One is ever one and never two."*<sup>[1](#_1)</sup>

*A prisoner in flesh, <u>you</u> comprehend?*

*Ex nihilo, a mustard seed;*<sup>[2](#_2)</sup> *I grew*

*The cosmos end to end to without end.*

*Who set the thumb to perfectly oppose,*

*To set in place a single errant hair?*

*Who quickens life within the womb\'s repose,*

***E**ffecting life in water as in air?*

*Who taught the foot to jerk and pull away*

*Before the pain, when just pricked by a pin?*<sup>[3](#_3)</sup>

*Who sets the stars above in their display,*

*Illuminates the moral sense within?*<sup>[4](#_4)</sup>

*I was, I am, I shall—eternally,*

<p><router-link to="/yellow-15"><i>When time and separation</i></router-link><sup><a href="#_5">5</a></sup> <router-link to="/yellow-15"><i>cease to be.</i></router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        God answers Job. See Job 38-39. The Creator is characterized by
        unity; creation is characterized by duality (such as wave-particle
        duality).
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        Nachmanides, in <i>Toras Hashem Temima</i> writes that God created
        the world from absolute nothingness. "The thing that was created
        was a small object that was as small as a mustard seed; this was the
        heavens and everything in them." This description is similar to
        that of the standard model Big Bang theory.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        When the foot is pricked by a pin, the leg pulls away even
        before pain is perceived.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        Cf. Immanuel Kant, Critique of Practical Reason: "Two things
        fill the mind with ever-increasing wonder and awe, the more often
        and the more intensely the mind of thought is drawn to them: the
        starry heavens above me and the moral law within me."
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        The essence of space is that it distinguishes, that is, it
        separates otherwise identical simultaneous events.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="yellow" to="/yellow-13">Previous Poem</router-link>
  <router-link class="yellow" to="/yellow-15">Next Poem</router-link>
</div>

<br>

<AmazonLink />