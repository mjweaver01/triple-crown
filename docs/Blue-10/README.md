---
title: God’s Blue Throne
meta:
    - name: description
      content: Triple Crown - God’s Blue Throne
    - name: keywords
      content: Triple Crown - God’s Blue Throne
---

# God’s Blue Throne

![](./media/image11.png)

The land is blanketed with grass and trees

But briefly. Then within its shroud of snow

It lies entombed in ice amid the freeze,<sup>[1](#_1)</sup>

Inert, dim light above, and none below.

Its lifeless monuments, the craggy heights,

Hold sway above the silent ice-draped plains,

While fossils of forgotten trilobites

**E**rode into unnumbered limestone grains

And blow to sea. But ah! the sea, the sea!

The sea has ever lived, has never ceased.

Its plankton exhale life at God's decree;<sup>[2](#_2)</sup>

Its depths provide our food and promised feast.<sup>[3](#_3)</sup>

God makes the sea abundant, vital, rife;

<p><router-link to="/blue-11">He fills the azure seas with teeming life.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        This poem describes the Ice Age.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        The primary source of oxygen in the atmosphere is plankton.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        See Talmud Bavli <i>Bava Basra</i> 75a. "The Holy One, blessed is He,
        will one day make a meal for the righteous from the flesh of the
        Leviathan."
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="blue" to="/blue-9">Previous Poem</router-link>
  <router-link class="blue" to="/blue-11">Next Poem</router-link>
</div>

<br>

<AmazonLink />