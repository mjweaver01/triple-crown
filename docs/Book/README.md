---
title: Book
sitemap:
  exclude: true
meta:
    - name: description
      content: Triple Crown
    - name: keywords
      content: Triple Crown
---

# Triple Crown

### by Eric Chevlen

## Introduction

*Triple Crown* is, to my knowledge, the only triple heroic crown of
sonnets in the English language. I believe it is the only such poetic
work in any language. Therefore, some words of introduction are
certainly appropriate, probably helpful, and possibly necessary.<sup>[1](#_1)</sup>

A crown of sonnets is a series of sonnets linked one to another, such
that the last line of one poem is repeated as the first line of the
subsequent poem. This process continues until the loop is closed by the
last line of the last poem's being repeated as the first line of the
first poem. A classic example of this is the exquisite seven poem crown
of sonnets by John Donne entitled, aptly enough, *La Corona.*

A heroic crown of sonnets is a sonnet crown consisting of fourteen poems
linked as described above. The added feature which makes the heroic
crown more than simply a longer collection of poems is that the linking
(repeated) lines of poetry themselves comprise another sonnet. Thus, a
fifteenth sonnet is implicit in the fourteen sonnets explicitly stated
in the heroic crown. This fifteenth sonnet is called the magistral
sonnet. As an added feature, the magistral sonnet is often an acrostic,
with the acronym embedded within it pithily summarizing the theme of the
entire set.

One can represent a heroic crown of sonnets, then, as a series of small
filled circles linked together by a larger circle, something like beads
on a necklace. It will be readily apparent that one could have three
such circles, with each circle intersecting the other two at two places.
One of the sites of intersection is common to all three circles. (See
figure below.) Each cycle of poetry, therefore, will contain three poems
whose first lines also serve as first lines in poems of the other two
cycles. This is the fundamental structure of *Triple Crown*.

![](./media/image1.png)

*Triple Crown* contains several other structural features. Naturally,
the magistral sonnet of each cycle still serves as an acrostic.
Additionally, the eighth line of each of the poems (excluding the
magistral sonnets) also form an acrostic spanning the entire poetic
work. In the text, the first letter of these lines is in bold font.

The three cycles of *Triple Crown* are not merely structured
thematically; they are also structured chromatically. Indeed, the
chromatic structure reflects the thematic structure.

Blue is the color ascribed to heaven and all that emanates from it. The
magistral sonnet of the blue cycle is a recapitulation of the story of
Creation, just as the poems within that cycle tell the story in greater
detail. Every poem of the blue cycle, except as noted below, refers to
some blue aspect of Creation.

Red represents conflict. The red cycle tells the stories of conflict in
the lives of the three great kings of Israel―Saul, David, and Solomon.
The magistral sonnet of that collection retells the story found in *Song
of Songs*; that great poem, too, tells of the conflict―love,
estrangement, and reconciliation―of God and the Jewish people. The poems
of the red cycle, again with the exception noted below, reveal the
redness of their subject.

Yellow is the color of dust, decline, and decay. The magistral sonnet of
the yellow cycle summarizes the story of Job, and the poems of that
cycle tell Job's story in greater detail. These poems, like those of the
other two cycles, reflect their thematic color, except as noted below.

The exception to the color coding of each cycle is the result of the
cycles intersecting. Where the blue and red cycles intersect, the
thematic color must be purple. Where the blue and yellow cycles
intersect, the thematic color must be green, and of course the
intersection of the red and yellow cycles is portrayed, however
implicitly, by the color orange. Where all three cycles intersect, the
colors combine to make white light. The poems at that unique
intersection are not sonnets, but villanelles.

Creation is known from afar. The blue cycle portraying Creation is
therefore written primarily in the third person. Conflict is dyadic. The
red cycle, therefore, is excerpts of dialogue, written primarily in the
second person. Suffering is personal. The yellow cycle is written in the
first person. There are, of course, many more structural and thematic
features in *Triple Crown*. I leave it to the reader to find or invent
these, and to enjoy them.

Above each poem in the book is a diagram indicating where, in the total
structure, the poem lies. This should help keep the reader apprised as
to his location within the entire work.

*Triple Crown* is not a collection of poems. It is a unified work. It is
intended to be read in the order presented. It merits re-reading.<sup>[2](#_2)</sup>

## God’s Blue Throne 1

![](./media/image2.png)

**G**randiloquent, creating with just speech,

**O** God, whom angels praise by day and night,

**D**eign these crowns, too, be worthy in Your sight.

**S**afeguard them nigh, and me, within Your reach.

**B**efore the force of nature was unleashed,

"**L**et there be light," He said, and there was light.

(**U**p to us, the sparks to reunite.)

**E**voked by love, the waves caress the beach.

**T**he land is blanketed with grass and trees.

**H**e fills the azure seas with teeming life,

**R**oaring beasts on land, with speech unblessed.

**O**ver all His creatures, land and seas,

**N**ature\'s lord He sets, the man and wife.

**E**vermore and more, God grants us rest.

## God’s Blue Throne 2

![](./media/image3.png)

Grandiloquent,<sup>[3](#_3)</sup> creating with just<sup>[4](#_4)</sup> speech,

The echo of His first substantial<sup>[5](#_5)</sup> word,

Extending from creation\'s farthest reach

Suffuses our blue world, and still is heard.<sup>[6](#_6)</sup>

Its symmetry, established from the first,

Gives witness to inflation\'s breathless run,<sup>[7](#_7)</sup>

When all that is, or ever will be, burst,

**W**hen all the universe, like God, was one.

But now the echoes' waves are nearly still,

Perhaps would be the absolute of cold,<sup>[8](#_8)</sup>

If God did not observe them by His will,<sup>[9](#_9)</sup>

And constantly rejuvenate the old.

We too exist, Lord, only in Your sight,

O God, whom angels praise by day and night,

## God’s Blue Throne 3

![](./media/image4.png)

O God, whom angels praise by day and night,<sup>[10](#_10)</sup>

A poor *paytan*<sup>[11](#_11)</sup> approaches Your blue throne<sup>[12](#_12)</sup>

To offer up before Your grandeur's height

These woven words,<sup>[13](#_13)</sup> these words of worth unknown.

O King, who was, who is, who e'er will be,

You gave three crowns,<sup>[14](#_14)</sup> a gift forevermore:

First priesthood's crown to Aaron's progeny,

**I**n purity, forgiveness to implore,

Then crown of Torah gave to rectify

Your people, and the nations if they heed.

Though now in dust the kingship crown may lie

It yet will gleam on David and his seed.

Now humbly I Your lofty gifts requite;

Deign these crowns, too, be worthy in Your sight.

## God’s Blue Throne 4

![](./media/image5.png)

Deign these crowns,<sup>[15](#_15)</sup> too, be worthy in Your sight,

These little crowns of ringlets yet unshorn,<sup>[16](#_16)</sup>

The foolishness of age,<sup>[17](#_17)</sup> and its delight,<sup>[18](#_18)</sup>

The love of children's children yet unborn.

They too may weave their crowns of praise in verse,

Amid life's multiplicity know One\...<sup>[19](#_19)</sup>

Or—O forbid!—soul-sicken, hate and curse

**T**he day that first was heard "Behold your son!"<sup>[20](#_20)</sup>

Each generation weaves its crown anew.

Each generation adds its twisted thread,

Or sets upon the crown a jewel of blue

Amid the strands of gold and brass and lead.

Inured to the despair<sup>[21](#_21)</sup> that life would teach,

Safeguard them nigh, and me, within Your reach.

## God’s Blue Throne 5

![](./media/image6.png)

Safeguard them nigh, and me, within Your reach.

Safeguard these sparks primaeval, holy sparks<sup>[22](#_22)</sup>

Suffusing all the world, a world in each,

All top and bottom strange and charming quarks.<sup>[23](#_23)</sup>

Safeguard the scintillation<sup>[24](#_24)</sup> tinted green<sup>[25](#_25)</sup>

Which spans between creation and the pit.

Safeguard the holy glue<sup>[26](#_26)</sup> that binds unseen

**H**adrons,<sup>[27](#_27)</sup> heaven—songs done<sup>[28](#_28)</sup> and songs unwrit.

Safeguard us too as we fulfill the task

To elevate the sparks from matter coarse,

To blow on glowing embers, and to bask

In light reborn returning to its source.

Safeguard us. We repair a wall first breached<sup>[29](#_29)</sup>

Before the force of nature was unleashed.

## God’s Blue Throne 6

![](./media/image7.png)

Before the force of nature was unleashed

By Him whose nature knows of no before,<sup>[30](#_30)</sup>

He knew creation's purpose,<sup>[31](#_31)</sup> not yet reached,

A premonition then and evermore:

The sabbath rest, a foretaste of rebirth.<sup>[32](#_32)</sup>

He shaped the male and female and their need,

The animals and plants. He made the earth

**I**n plenitude, for sustenance and seed.

The sun and moon and stars He made from naught,

And supernovae's ancient brilliant blast,

That heavy elements might there be wrought.<sup>[33](#_33)</sup>

Timeless, He is and sees the first and last:

That two small flames of blue she might ignite,<sup>[34](#_34)</sup>

"Let there be light," He said—and there was light.

## God’s Blue Throne 7

![](./media/image8.png)

"Let there be light," He said, and there was light.<sup>[35](#_35)</sup>

The scattered sparks flew each its willful way.

(Up to us, the sparks to reunite.)

And there was evening, darkness renamed night,<sup>[36](#_36)</sup>

And there was morning on that unique day<sup>[73](#_37)</sup>

"Let there be light," He said, and there was light.

The spheres that held them shattered.<sup>[38](#_38)</sup>  Sparks in flight

**N**ow fled in hell-bent scattered disarray.

(Up to us, the sparks to reunite.)<sup>[39](#_39)</sup>

How glorious stretched eternity, and bright.

How pure and perfect past and future lay.

"Let there be light," He said, and there was light.

But pure and perfect bore within it blight;

Eternity proved mother to decay.

(Up to us, the sparks to reunite.)

Or is this greater glory, recondite,

A co-creating part for us to play?

"Let there be light," He said, and there was light.

(Up to us, the sparks to reunite.)

## God’s Blue Throne 8

![](./media/image9.png)

Up to us, the sparks to reunite

Into a realm of holiness on earth;

To raise, redeem, the scattered shards of light

Inherent, hidden in apparent dearth;

To gather them and us upon a shore

Whose edge of blue abuts a sunless sea;<sup>[40](#_40)</sup>

And send them soaring homeward; to restore

**T**o each the place where it was meant to be.

Each particle of light, a wave is too,<sup>[41](#_41)</sup>

And interferes with others in its course.<sup>[42](#_42)</sup>

To whom send such a message? Replies who?

Detected and reflected by what force?

And yet—like angels calling each to each,<sup>[43](#_43)</sup>

Evoked by love, the waves caress the beach.<sup>[44](#_44)</sup>

## God’s Blue Throne 9

![](./media/image10.png)

Evoked by love, the waves caress the beach.<sup>[45](#_45)</sup>

The beach, in turn, yields grain by grain her sand,

Like lovers wrapped in rapture, each in each:

The ageless love of water and of land.

In tidal pools, in basins, in the muds

Of swamps and marshes, prairies soaked with rain,

In gullies gouged by angry ancient floods,

**H**ere life may start, and end, and start again.<sup>[46](#_46)</sup>

Beneath the frost-tipped lilacs, earth is rife

With musty germinations. The untold

And ceaseless, restless writhing of new life

That stirs amid the rank decay of old

Lies hidden, fecundation no one sees:<sup>[47](#_47)</sup>

The land is blanketed with grass and trees.

## God’s Blue Throne 10

![](./media/image11.png)

The land is blanketed with grass and trees

But briefly. Then within its shroud of snow

It lies entombed in ice amid the freeze,<sup>[48](#_48)</sup>

Inert, dim light above, and none below.

Its lifeless monuments, the craggy heights,

Hold sway above the silent ice-draped plains,

While fossils of forgotten trilobites

**E**rode into unnumbered limestone grains

And blow to sea. But ah! the sea, the sea!

The sea has ever lived, has never ceased.

Its plankton exhale life at God's decree;<sup>[49](#_49)</sup>

Its depths provide our food and promised feast.<sup>[50](#_50)</sup>

God makes the sea abundant, vital, rife;

He fills the azure seas with teeming life.

## God’s Blue Throne 11

![](./media/image12.png)

He fills the azure seas with teeming life

But not His image.<sup>[51](#_51)</sup> That He would not keep

Amid the turbulence and tide, the strife

Of predator and prey within the deep.

Nor did He place His likeness in the breeze

Which blows alike for dust and cloud and hawk,

Indifferent, sowing pollen and disease.

**H**e chose the land—is He not called the Rock?—<sup>[52](#_52)</sup>

To bear His image, soil of every clime,<sup>[53](#_53)</sup>

And formed the creature there upon the block

Where He will set His house again in time.<sup>[54](#_54)</sup>

He gave him speech. His creature-child can talk

And pray, and curse. Not so the rest:

Roaring beasts on land, with speech unblessed.

## God’s Blue Throne 12

![](./media/image13.png)

Roaring beasts on land, with speech unblessed,

Exemplify in awe their Maker's might,

While humble creatures likewise manifest

The attributes that honor the upright.<sup>[55](#_55)</sup>

In modesty the cat heeds nature's call.

The ant will not purloin his fellow's grain.

In chastity the dove instructs us all

**I**n how to mate for life, and so remain.

The rooster, in his wooing of the hen,

Reveals the innate wisdom that he knows—

Within a turquoise palace or a pen—

That every cock should coo before he crows.

God\'s providence extends, like His decrees,

Over all His creatures, land and seas.

## God’s Blue Throne 13

![](./media/image14.png)

Over all His creatures, land and seas,

A pair and paradox He sets to reign,

The creatures who reach higher on their knees,<sup>[56](#_56)</sup>

Whose aggrandizement ever is in vain.<sup>[57](#_57)</sup>

To know of good and evil and its tree,

They eat its fruit, and open wide their eyes,<sup>[58](#_58)</sup>

No longer good, no longer evil see

**D**iscrete, but good-and-evil in disguise.<sup>[59](#_59)</sup>

A paragon<sup>[60](#_60)</sup> and paradox is man,

Alone allowed approach to God's blue throne,

Alone enabled to defy His plan,

Alone a soul interred in flesh and bone.

One soul placed in two bodies for their life,<sup>[61](#_61)</sup>

Nature's lord, He sets the man and wife.

## God’s Blue Throne 14

![](./media/image15.png)

Nature's lord He sets, the man and wife,

Into a paradise of their demesne,

But disobedience<sup>[62](#_62)</sup> and causeless strife<sup>[63](#_63)</sup>

Expel them to the thrall of death and pain,<sup>[64](#_64)</sup>

She, to bear her cyanotic young,

In clutch-throat anguish whisper, "Breathe, my child!"

And he, amid the brow-sweat, mud, and dung,

**D**ethroned as Eden's king, to tame the wild.

And yet\...the source of blessings nonetheless

Ordains a day when tribulations end,

A day that man or God or both may bless,

And spirit over matter yet transcend:

No more weak days<sup>[65](#_65)</sup> in grubbing futile quest;

Evermore and more, God grants us rest.

## God’s Blue Throne 15

![](./media/image16.png)

Evermore and more, God grants us rest

From gleaning, 'mid the dust and broken clay,

The crumbs of daily crust which may, at best,

Suppress the pangs of hunger for the day.

Creation's last blue day, when we have done

The work that God created us to do

Reintegrates the broken parts to One

**E**ternal, unifying sparks anew.

O holy day beyond the mortal ken!

O day we struggle all our days to reach!

O day when we may sigh a last amen,

Restore at last the long neglected breach.<sup>[66](#_66)</sup>

No more to live in yearning, I beseech,

Grandiloquent, creating with just speech.

**O**h let us be united, arms outreach,

**H**ear songs of love, and poetry recite—

**Z**ither, myrrh, red wine—ev'ry delight

**E**voked by love.  The waves caress the beach,

**A**s paradise regained swells into reach.

\"**L**et there be light," He said, and there was light\...

**O**nly once, this dark, this moonless night

**T**he barren walls reverberate a screech—

**S**o cold a night, and none to warm my bed.

**S**o long a night, I scarce recall the days

**O**f cedar paneled chambers and the vow.

**R**epeat it now exactly as you said—

**E**ternally, forever and always!

**D**elay no moment longer.  Take me now!

## Red 1

![](./media/image17.png)

"Oh let us be united, arms outreach!"<sup>[67](#_67)</sup>

You greet me *thus*? Your greeting only mocks

*Your* broken state, resounds the wretched bleats

Of pilfered herds forbidden and the flocks<sup>[68](#_68)</sup>

Of reddled rams<sup>[69](#_69)</sup> consigned to death.  See there

Your enemy in chains.<sup>[70](#_70)</sup>  And yet, this day will bring

For *you* a broken link,<sup>[71](#_71)</sup> for *him* an heir,<sup>[72](#_72)</sup>

**N**or spare my robe,<sup>[73](#_73)</sup> your kingdom, or that king.

Oh Saul, my Saul, if only you had been

More than a donkey-chaser in your eyes,<sup>[74](#_74)</sup>

You might have lived in full what life can mean.

Now go—and live a life you must despise:

Go wallow in all corporal delight,

Hear songs of love, and poetry recite.<sup>[75](#_75)</sup>

## Red 2

![](./media/image18.png)

Hear songs of love, and poetry recite—<sup>[76](#_76)</sup>

And still you'll not escape your rival's hand;

That son of Jesse will not cease to fight

Until he seize your crown, your throne, your land.<sup>[77](#_77)</sup>

And tell me, now, where is your sword, your bow,

Your robe which showed your place above the ranks?<sup>[78](#_78)</sup>

Not traded for a death blow from the foe—

**M**uch worse: you swapped them for a shepherd's thanks.

Ungodly gift,<sup>[79](#_79)</sup> a man who pulls down shame

On all our tribe—my son!  Oh what a fall

Is yours. You could have been a king, a name

To long endure. You could have had it all:

Kings to crawl and grovel to your might,

Zither, myrrh, red wine—ev'ry delight.

## Red 3

![](./media/image19.png)

Zither, myrrh, red wine, ev'ry delight

Betray me too, refuse to satisfy,

Assault my hearing, smell, my taste and sight,<sup>[80](#_80)</sup>

As you, in murmurs, plot to nullify

My battered throne. You crave to do me wrong.

Even village girls with dusty feet

Conspire to mock my conquests with their song,<sup>[81](#_81)</sup>

**E**masculate my triumph as defeat.

The blood of my slain thousands flows to sea

To feed their fish-god deep beneath the waves.<sup>[82](#_82)</sup>

How fine an ending that—to cease to be,

To wash the dust from off a soul that craves

No more, of God himself be out of reach.

Evoked by love, the waves caress the beach.

## Red 4

![](./media/image20.png)

Evoked by love, the waves caress the beach,<sup>[83](#_83)</sup>

Erasing from the sand all mortal stain

Of struggle and the bleedings that will reach

It, washed in time by river and by rain.

And that will be our end, to perish here

Among these purple irises in bloom,<sup>[84](#_84)</sup>

With blood-stained grass to serve us as a bier,

**A**nd flowers crushed to be our only tomb.<sup>[85](#_85)</sup>

And I shall yield my kingdom and my crown—

Yes, I who lost them for a tattered hem—<sup>[86](#_86)</sup>

And like these iris petals lay me down

For all to pluck, impaled upon a stem,<sup>[87](#_87)</sup>

As thirsty swords our waning life-force leech,

As paradise regained swells into reach.

## Red 5

![](./media/image10.png)

*As paradise regained swells into reach,*<sup>[88](#_88)</sup>

*He rouses* *horded treasure of my scent.*

*The seed that lies within a ruby peach,*

*So was my hero husband in my tent.*

But now you hike your tunic,<sup>[89](#_89)</sup> raise your leg

Like mongrel dogs that piss against a wall.<sup>[90](#_90)</sup>

My daddy earned respect you cannot beg,

**N**or ever did he stoop to be so small.<sup>[91](#_91)</sup>

You were not fit to even touch his hem.

How dare you snip his cloak<sup>[92](#_92)</sup>—a king like him!

Your beddings, cold, deny you and condemn.<sup>[93](#_93)</sup>

*He was the bridegroom in my chamber dim,*

*And I the virgin princess clad in white.*

*"Let there be light," He said. And there was light.*

## Red 6

![](./media/image21.png)

"Let there be light," He said, and there was light,<sup>[94](#_94)</sup>

But you enticed me and I went astray<sup>[95](#_95)</sup>

Only once, this dark, this moonless night.

You snuffed my lamp; your shadow veiled my sight,

And so unseen I could not see the way

"Let there be light," He said, and there was light.

Your ribbons wrapped about me, lacing tight;<sup>[96](#_96)</sup>

**I**nto the depths I sank, could barely pray

Only once, this dark, this moonless night.

God answered me expansively and bright,<sup>[97](#_97)</sup>

In brilliance well beyond the squint of day.

"Let there be light," He said, and there was light.

Reflecting, newly chastened and contrite,

My backward glance may briefly me betray,

Only once, this dark, this moonless night.

Though I, in twilight, penances recite,<sup>[98](#_98)</sup>

The image of that night does not decay:

"Let there be light," He said, and there was light

Only once, this dark, this moonless night.

## Red 7

![](./media/image8.png)

Only once, this dark, this moonless night<sup>[99](#_99)</sup>

He reached out, quite by habit—she was gone,

Who'd shared his bread, his cup, his bed.<sup>[100](#_100)</sup>  Delight

Itself the thief had grabbed. What's to be done

To such a one, my king—how do you hold?

Yes, justice will proceed just as you plan;

He merits death, must compensate fourfold.<sup>[101](#_101)</sup>

**N**ow one more thing, my king:  *You* are the man!<sup>[102](#_102)</sup>

Anointed one of God, you have forsaken

God, and pampered evil in your heart.

Now from your sundered house will four be taken,<sup>[103](#_103)</sup>

The blood-stained dripping sword will not depart.<sup>[104](#_104)</sup>

When justice to the battlements will reach,

The barren walls reverberate a screech.

## Red 8

![](./media/image22.png)

The barren walls reverberate a screech—<sup>[105](#_105)</sup>

My son, my son, O Absalom, my son!<sup>[106](#_106)</sup>

But daddy's lullabies will no more reach

My son, my son, O Absalom, my son!

No more, my son, I'll peel the prickly pear<sup>[107](#_107)</sup>

For you, no more embrace you while my fingers

Plow love furrows through your crown of hair.<sup>[108](#_108)</sup>

**G**one is hope, my son,<sup>[109](#_109)</sup> though yearning lingers.

Gone too those chilly nights when you would climb

Into my bed, and curl beside me, gently snore,

And warm yourself and me at the same time.

Such nights of warmth and whispers—nevermore!

Reverberant, this night, with words unsaid;

So cold a night—and none to warm my bed.

## Red 9

![](./media/image23.png)

So cold a night, and none to warm my bed<sup>[110](#_110)</sup>

But you, a youthful stranger hired to spend

The night, to hear my stories dear and dread,

And nod and wonder too when will it end.<sup>[111](#_111)</sup>

I killed a giant once,<sup>[112](#_112)</sup> six cubits high

Or more,<sup>[113](#_113)</sup> cut off his bloody head and raised

It by the hair.<sup>[114](#_114)</sup>  But surely that's not why—

**O** Absalom!  God's anger also blazed

Against the baby.<sup>[115](#_115)</sup>  Better *I* should die.

O Absalom!   I'll go to him; he will 

Not come to me.<sup>[116](#_116)</sup>  Michal, would you deny

A king?<sup>[117](#_117)</sup>  Oh, Jonathan, my escort still?<sup>[118](#_118)</sup>

I see you gathered, all, through murky haze.

So long a night, I scarce recall the days.

## Red 10

![](./media/image24.png)

*So long a night, I scarce recall the days*<sup>[119](#_119)</sup>

*Before ambition seized my widowed heart.*

*Yes, open, lord, my lips; I'll tell your praise.*<sup>[120](#_120)</sup>

*A girl of seven years can play this part.*<sup>[121](#_121)</sup>

Remember, husband-king, the vow you made

Before we wed: "Your son will reign," you said,<sup>[122](#_122)</sup>

"When, with my fathers, I at last am laid

**F**orever in my shroud and cedar bed.\"

Now may you live forever, O my lord.

Now crown my son and seat him on your chair,

Your red-eyed wife and nation have implored.

*While still your life hangs by its brittle hair,*<sup>[123](#_123)</sup>

*Let only these two thoughts your mind allow:*

*Of cedar paneled chambers,*<sup>[124](#_124)</sup> *and the vow.*

## Red 11

![](./media/image25.png)

Of cedar paneled chambers,<sup>[125](#_125)</sup> and the vow 

That Joab swore, and how your foe did cling

In vain to our most holy shrine,<sup>[126](#_126)</sup> I'll now

Relate, my lord—and may your servant bring

A smile of pleasure to your face.<sup>[127](#_127)</sup>  He swore

He'd never leave that place—and it was so.<sup>[128](#_128)</sup>

Defile the altar with his very gore—

**T**hat was his plan—and with that same blood flow

To stain your name.  He mocked you, said the sword

Devours this way and that,<sup>[129](#_129)</sup> and now, withdrawn

From him, his curse inures to you, my lord:

Impure and leprous, lame and hungry spawn.<sup>[130](#_130)</sup>

I struck—and summoned him as he lay dead,

\"Repeat it *now*—exactly as you said.\"

## Red 12

![](./media/image26.png)

"Repeat it now exactly as you said."<sup>[131](#_131)</sup>

I told them that for you, Zabud, not me.

Which mother had the live son, which the dead,

Was clear to me when each had made her plea.<sup>[132](#_132)</sup>

But how to get my court to know that fact?<sup>[133](#_133)</sup>

I ordered that the boy be cut in two,

And both of them were certain to react

**E**xactly as they did for all to view.<sup>[134](#_134)</sup>

The wisdom of a king is what I sought,<sup>[135](#_135)</sup>

To know the reddened hand and icy heart,

And not forsake the lesson mother taught.<sup>[136](#_136)</sup>

A king, in wisdom, also plays a part.

Such burning wisdom in a king must blaze

Eternally, forever and always.

## Red 13

![](./media/image27.png)

Eternally, forever, and always—<sup>[137](#_137)</sup>

And no thing new is seen beneath the sun.

The endless shuffling march of nights and days:

What was, will be; what will be has been done.<sup>[138](#_138)</sup>

You gave me wisdom, Lord, as I had sought,

And made me rich in flocks and oil and grain.

To what avail?  For all of this is naught,

**N**or ever will be more, for all is vain.<sup>[139](#_139)</sup>

The wise man and the fool have the same end.<sup>[140](#_140)</sup>

All laughter, wailing, bleeding likewise cease.

What misers horde, another man will spend,<sup>[141](#_141)</sup>

And even in the grave is doubtful peace.<sup>[142](#_142)</sup>

Before your pow'r to nullify I bow.<sup>[143](#_143)</sup>

Delay no moment longer.  Take me now!

## Red 14

![](./media/image28.png)

Delay no moment longer.  Take me now

To see the men at arms who guard the land...<sup>[144](#_144)</sup>

You soldiers know what threats we face, and how

To fight.  Now shout:  United! Arms in hand!

Our enemies would cut our land in two,

Would spill our babies' blood, and their war cry

Would rend the air.  But this they will not do!

**L**et us shout:  United! Arms held high!

United shall we pacify our land

From Dan unto Beersheba,<sup>[145](#_145)</sup> and our reach

Extend from our great river to the strand

Where pummel salty waves upon the beach.<sup>[146](#_146)</sup>

One God!  One land!  One people without breach,

Oh let us be!  United!  Arms outreach!

![](./media/image29.png)

## Yellow 1

![](./media/image30.png)

**D**esire itself no longer within reach,

**U**nstinting and severe, the hands that smite—

**S**uch justice, and its judge, do I indict.

**T**he barren walls reverberate a screech

**A**s muzzy-mumble sages nod and teach

"**L**et there be light," He said, and there was light.

**L**ike scattered stars abandoned to the night,

**S**afeguard them nigh—and me within your reach

**Y**ou may destroy, completely extirpate.

**E**rase the yellowed parchment, start anew.

**L**ament some other human tragedy,

**L**est man believe that he is thrall to fate.

**O**ne is ever one and never two,

**W**hen time and separation cease to be.

## Yellow 2

![](./media/image31.png)

Desire itself no longer within reach,<sup>[147](#_147)</sup>

I still recall when I could feel its grip,

When any wanted thing, mere whispered speech

Would place into my hand, nor would it slip

From out my grasp.<sup>[148](#_148)</sup>  And more:  I was content!

I did not know the maggot gnaw of greed,

But knew the joy of gold and time well spent.

**I** knew the gratitude of those in need

Of generosity with wealth to match.<sup>[149](#_149)</sup>

Now gone, all gone!  All felled within an hour!

The forces of misfortune thus could snatch

My slow accreted honor, wealth, and pow'r.

The brightness of my day enshrouds my night.

Unstinting and severe, the hands that smite.

## Yellow 3

![](./media/image32.png)

Unstinting and severe, the hands that smite,

That strangle the newborn with its own cord,

That justify injustice with just might:

I summon now to justice nature's lord.<sup>[150](#_150)</sup>

I witness—I alone am left to tell—

That ten were wrenched like fingers from my hands

And crushed together, buried where they fell,

**E**ntombed beneath the stones and yellow sands.<sup>[151](#_151)</sup>

No daughter\'s corpse was left me to inter,

Nor son to bury me.  Ten children—dead!

Then came my friends to comfort, to confer

Their peace on me.<sup>[152](#_152)</sup>  "God's ways are just" they said,

"He rules the world with justice and with might."

Such justice, and its judge, do I indict.

## Yellow 4

![](./media/image33.png)

Such justice, and its judge, do I indict:

Omnipotent, inscrutable, and far

From reason to or reason not to smite

The flesh in which we live or which we are.<sup>[135](#_153)</sup>

My flesh, a charnel feast where flies may spawn,

Precedes me into death.  I sit in dust,

Ooze life, and with a greasy ostracon<sup>[154](#_154)</sup>

**S**crape from my wounds repugnant honey crust.<sup>[155](#_155)</sup>

O flesh so fair, so faithless to your form,

So foul and fetid now, a wounded field,

Repulsive to all life except the worm!

And what avail my prayers?  He is concealed.<sup>[156](#_156)</sup>

No answer comes whence imprecations reach.

The barren walls reverberate a screech.

## Yellow 5

![](./media/image23.png)

The barren walls reverberate a screech

Of frantic ululation, words aflame.

The barren mother,<sup>[157](#_157)</sup> desp\'rate, claws to reach

For cause, for purpose, meaning—or for blame.

\"Disease and dispossession, graceless death

Do not befall the sinless.  While I live,"

She says, "I hate you, 'til my final breath,

**E**xcoriate your name, and not forgive.

Curse God and die."<sup>[158](#_158)</sup>  She never speaks again.

We eat our lentils silently and cold,<sup>[159](#_159)</sup>

And silently and cold she knows my pain

Is more than she can bear, and she has told

The bold and dread conclusion I must reach,

As muzzy-mumble sages nod and teach.

## Yellow 6

![](./media/image34.png)

As muzzy-mumble sages nod and teach

Of worlds they've never seen, they promise there

Is found the world of truth.  And as they preach,

I scrape my skin in loathing and despair.

Here is the truth: the yellow scum I scrape

From stinking flesh.  Here is the truth:  the loss

Of all I was, the truth none can escape,

**R**eality revealed in all its dross.

The death of hope illuminates the world,

Allows no further loss, no more deceit,

No blessings, curses, imprecations hurled,

No more illusions ceaseless to repeat.

The truth is revelation blinding bright.

"Let there be light," He said, and there was light.

## Yellow 7

![](./media/image8.png)

"Let there be light," He said, and there was light,

So long ago and now so far away,

Like scattered stars abandoned to the night.

The world, which once we thought could hold delight,

A wasteland now, it mocks the wasted day

"Let there be light," He said, and there was light.

And we too have been banished from His sight,

**I**nvisible, to blindly feel our way,

Like scattered stars abandoned to the night.

Nor shall we know redemption from our plight,

Nor does this hoary fact our grief allay:

"Let there be light," He said, and there was light.

Like dust indifferent wind blows from a height,

We float unseen, and slowly drift away,

Like scattered stars abandoned to the night.

How grand was the creation—and how slight

The part, if any, we are called to play.

"Let there be light," He said, and there was light,

Like scattered stars abandoned to the night.

## Yellow 8

![](./media/image35.png)

Like scattered stars abandoned to the night,

Too feeble the great darkness to adorn,

Now come my friends to loom athwart my light,

To triply crown my sorrow with their scorn.

"The world was not created," says the first,<sup>[160](#_160)</sup>

"To be surrendered, sundered in the maw

Of chaos.  Rather, from the primal burst

**C**ontinues on its course and by its law.

The close of day, the yellow autumn's end,

The crumbling of a wall, a kingdom's fall,

Our shabby motley fabric and its rend

Convince the fool that chaos governs all.

The wise will see this, bow in awe, beseech,

'Safeguard them nigh, and me, within your reach.'"

## Yellow 9

![](./media/image6.png)

Safeguard them nigh, and me, within your reach—

A child's prayer before he goes to bed,

The twice-chewed cud of empty prattle speech

Before his throat is clutched in claws of dread.

Oh, yes, the world has laws by which it\'s run

In mulish bondage to its master's plan.

But justice! Where is justice? There is none!<sup>[161](#_161)</sup>

**C**ondemned to live in vain and die is man,

For he alone has knowledge of his fate,

And will to will the world as it is not.

For every day's tomorrow must he wait,

And every yesterday relive in thought.

And me, O God, whose death will come too late,

You may destroy, completely extirpate.

## Yellow 10

![](./media/image36.png)

"You may destroy, completely extirpate,"

Replies the second friend, "That final spark

Still glowing in your soul, ascribe to fate

The recompense of man, and in your dark

Cascade deny the light.<sup>[162](#_162)</sup>  This you may do 

Oblivious to truth.  The blind deny

The light while staring at the sun—and you

**H**owl 'Justice!' just as justice you defy.

Yet consequence is plain to see for all,

Unscrolled in time, writ large and fair, condign.

Your scroll, though now a smudged and angry scrawl,

Could be a palimpsest of your design.

Your plaint is common, shameful, and untrue.

Erase the yellowed parchment, start anew.\"

## Yellow 11

![](./media/image37.png)

Erase the yellowed parchment, start anew—

A foolish hope, a vain and futile goal.

I am the sum of all I've suffered through;

Without my scars I'd be another soul.

No, better I had died before my birth,<sup>[163](#_163)</sup>

Or swaddling cloths had been my tiny shroud—

I'd not complain of justice and its dearth.

**E**xistence, to be honest, is not cowed,

But howls the hated truth though none believe.

Myself alone I know, and you do not,

And to unsullied innocence I cleave

In isolation by injustice wrought.<sup>[164](#_164)</sup>

I cannot, till my justice come to me,

Lament some other human tragedy.

## Yellow 12

![](./media/image38.png)

"Lament some other human tragedy,"

Replies my final friend.  "No more assault

Our ears with self-indulgent threnody

For losses which, in truth, are your own fault.<sup>[165](#_165)</sup>

You did not rail against divine decree

While basking in your health and wealth and name;

Now just conviction makes you disagree.

**V**ituperate yourself, for you're to blame.

You planted vetch, and now would harvest grape,

Sinned secretly, now innocence declaim.

The honey-crusted oozings that you scrape

Make evident your guilt, if not your shame.

Confess. Repent.<sup>[166](#_166)</sup> No longer desecrate,

Lest man believe that he is thrall to fate."

## Yellow 13

![](./media/image39.png)

Lest man believe that he is thrall to fate.

Or history unfurls without an aim,

I call on God.  Appear—and advocate

My claim of innocence.  Or if You blame

Me for transgressions, state Your case.<sup>[167](#_167)</sup>  Remind me

Of Your storied truth, at last reveal

Your hidden justice, tell me why You grind me

**L**ike a pollen grain beneath Your heel.

You are one, alone, unique in awe;

I am one, alone, unique in woe.

Establish justice, melding truth with law,

Not just in lofty heights, but here below.

Duplicity deny and faith renew.

One is ever one and never two.

## Yellow 14

![](./media/image40.png)

*\"One is ever one and never two.\"*<sup>[168](#_168)</sup>

*A prisoner in flesh, <u>you</u> comprehend?*

*Ex nihilo, a mustard seed;*<sup>[169](#_169)</sup> *I grew*

*The cosmos end to end to without end.*

*Who set the thumb to perfectly oppose,*

*To set in place a single errant hair?*

*Who quickens life within the womb\'s repose,*

***E**ffecting life in water as in air?*

*Who taught the foot to jerk and pull away*

*Before the pain, when just pricked by a pin?*<sup>[170](#_170)</sup>

*Who sets the stars above in their display,*

*Illuminates the moral sense within?*<sup>[171](#_171)</sup>

*I was, I am, I shall—eternally,*

*When time and separation*<sup>[172](#_172)</sup> *cease to be.*

## Yellow 15

![](./media/image41.png)

When time and separation cease to be

A stranglehold unstinting and severe

Athwart the very breath that breathes in me,

The timeless far is now so very near.

And poverty no longer makes me poor,

And loneliness no longer all alone.

The pain, unchanged, persistent, without cure,

**N**o longer makes me suffer, though I groan.

Omnipotent You are, as I have heard,

But now my eyes have seen You, and your speech

Grandiloquent to me excels Your word.

If in this yellow dust the sun would bleach

My bones, I\'m still content, repaired the breach,

Desire itself no longer within reach.<sup>[173](#_173)</sup>

<br>

## Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        The adverbs here decrease in intensity as the adjectives increase
        in intensity. A similar pattern is seen in Psalms 1:1. "The praises
        of Man are that he walked not in the counsel of the wicked, and
        stood not in the path of the sinful, and sat not in the session of
        scorners." Walking by gives less exposure than standing, and
        standing gives less exposure than sitting. The wicked are worse than
        the sinful, and the sinful are worse than the scorners.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See author's introduction to <strong>The Path of the Just</strong> (<strong>Mesillat
        Yesharim</strong>), by Rabbi Moshe Chayim Luzzatto.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        The first word of the poem is itself grandiose to suggest the
        grandeur and eloquence of the word by which the world was created.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        Throughout most of this book, the word "just" is ambiguous as to
        whether it means "rightful," "merely," or both.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        The first words—"Let there be light"—created substance.
    </p>
</div>

<div class="footnote">
    <p id="_6">
        <sup>6</sup>: 
        The background cosmic radiation is the observable remnant of the
        Big Bang.
    </p>
</div>

<div class="footnote">
    <p id="_7">
        <sup>7</sup>: 
        The observable universe is remarkably uniform on a large scale.
        Since distant parts of the universe cannot communicate within the
        time since the Big Bang, this uniformity is evidence that the
        universe expanded explosively in an inflationary period shortly
        after the Big Bang.
    </p>
</div>

<div class="footnote">
    <p id="_8">
        <sup>8</sup>: 
        The temperature of the cosmic microwave background radiation is
        less than three degrees (Celsius) above absolute zero.
    </p>
</div>

<div class="footnote">
    <p id="_9">
        <sup>9</sup>: 
        One of the tenets of quantum mechanics is that a quantum level
        event transitions from potential to actual only upon observation.
    </p>
</div>

<div class="footnote">
    <p id="_10">
        <sup>10</sup>: 
        See Babylonian Talmud (hereinafter referred to as Talmud Bavli)
        <strong>Chullin</strong> 91b. "And the angels do not sing above until Israel speaks
        below." Also see Isaiah 6:3 and Deuteronomy 6:7.
    </p>
</div>

<div class="footnote">
    <p id="_11">
        <sup>11</sup>: 
        A <strong>paytan</strong> is a person who writes <strong>piyyutim</strong> (singular,
        <strong>piyyut</strong>). A <strong>piyyut</strong> is a Jewish liturgical poem, usually based on
        a poetic scheme such as an acrostic.
    </p>
</div>

<div class="footnote">
    <p id="_12">
        <sup>12</sup>: 
        See Ezekiel 1:26.
    </p>
</div>

<div class="footnote">
    <p id="_13">
        <sup>13</sup>: 
        This is an allusion to <strong>Anim Zemirot</strong>, a <strong>piyyut</strong> attributed to
        the 12th century *paytan* Rav Yehudah HaChassid. Its opening lines
        are "I shall compose pleasant psalms, and weave together hymns."
    </p>
</div>

<div class="footnote">
    <p id="_14">
        <sup>14</sup>: 
        See <strong>Pirkei Avot</strong> 4:17. "Rabbi Shimon said, 'There are three
        crowns—the crown of Torah, the crown of priesthood, and the crown
        of kingship...'"
    </p>
</div>

<div class="footnote">
    <p id="_15">
        <sup>15</sup>: 
        This is an allusion to John Donne's sonnet crown *La Corona*. Its
        opening line is "Deign at my hands this crown of prayer and
        praise..."
    </p>
</div>

<div class="footnote">
    <p id="_16">
        <sup>16</sup>: 
        Among Chassidim it is customary not to cut a boy\'s hair until he
        is three years old. The first haircut is given religious
        significance in a ceremony called <strong>upsharnish</strong>.
    </p>
</div>

<div class="footnote">
    <p id="_17">
        <sup>17</sup>: 
        Grandparents lose their decorum around their young grandchildren.
    </p>
</div>

<div class="footnote">
    <p id="_18">
        <sup>18</sup>: 
        See Proverbs 17:6.
    </p>
</div>

<div class="footnote">
    <p id="_19">
        <sup>19</sup>: 
        See Zechariah 14:9.
    </p>
</div>

<div class="footnote">
    <p id="_20">
        <sup>20</sup>: 
        See Genesis 29:32.
    </p>
</div>

<div class="footnote">
    <p id="_21">
        <sup>21</sup>: 
        See Søren Kierkegaard, <strong>The Sickness Unto Death</strong>, concerning
        despair.
    </p>
</div>

<div class="footnote">
    <p id="_22">
        <sup>22</sup>: 
        It is a basic tenet of Lurianic Kabbalah that in the pre-temporal
        era of creation, God sent forth ten vessels of holy light, but the
        vessels shattered, scattering their sparks throughout the mundane
        world. The Ari (Rabbi Isaac Luria, 1534-1572) taught: "There is no
        sphere of existence, including organic and inorganic nature, that is
        not full of holy sparks which are mixed in with the <strong>kelippot</strong>
        [husks], and need to be separated from them and lifted up."
    </p>
</div>

<div class="footnote">
    <p id="_23">
        <sup>23</sup>: 
        Quarks are elementary particles and a fundamental constituent of
        matter. There are six types of quarks, known as flavors: up, down,
        strange, charm, top, and bottom.
    </p>
</div>

<div class="footnote">
    <p id="_24">
        <sup>24</sup>: 
        The process of scintillation is one of luminescence whereby light
        of a characteristic spectrum is emitted following the absorption of
        radiation.
    </p>
</div>

<div class="footnote">
    <p id="_25">
        <sup>25</sup>: 
        The green Earth exists as a temporal and spatial oasis between
        Creation and the final collapse of the solar system.
    </p>
</div>

<div class="footnote">
    <p id="_26">
        <sup>26</sup>: 
        Quarks are bound together by elementary particles called gluons
        to form neutrons or protons.
    </p>
</div>

<div class="footnote">
    <p id="_27">
        <sup>27</sup>: 
        Hadrons are elementary particles comprised of quarks and other
        elementary particles.
    </p>
</div>

<div class="footnote">
    <p id="_28">
        <sup>28</sup>: 
        An *homage* to the poetry of John Donne.
    </p>
</div>

<div class="footnote">
    <p id="_29">
        <sup>29</sup>: 
        See Isaiah 58:12. "Ancient ruins will be rebuilt through you, and
        you will restore generations-old foundation; and they will call you
        'repairer of the breach' and 'restorer of paths for habitation.'"
    </p>
</div>

<div class="footnote">
    <p id="_30">
        <sup>30</sup>: 
        See Isaiah 44:6.
    </p>
</div>

<div class="footnote">
    <p id="_31">
        <sup>31</sup>: 
        See Genesis 2:1-3. The Sabbath is the culmination of Creation,
        the only day which God both blessed and sanctified.
    </p>
</div>

<div class="footnote">
    <p id="_32">
        <sup>32</sup>: 
        See Talmud Bavli <strong>Berachos</strong> 57b, which cites the Sabbath as a
        semblance of the world to come.
    </p>
</div>

<div class="footnote">
    <p id="_33">
        <sup>33</sup>: 
        After the Big Bang, the material universe was hydrogen and
        helium. Heavier elements were subsequently created in the collapse
        of stars, which we perceive as supernovae.
    </p>
</div>

<div class="footnote">
    <p id="_34">
        <sup>34</sup>: 
        The two small flames are the Sabbath candles.
    </p>
</div>

<div class="footnote">
    <p id="_35">
        <sup>35</sup>: 
        See Genesis 1:3.
    </p>
</div>

<div class="footnote">
    <p id="_36">
        <sup>36</sup>: 
        See Genesis 1:5. Darkness existed before God named it "night."
    </p>
</div>

<div class="footnote">
    <p id="_37">
        <sup>37</sup>: 
        Ibid. The evening and morning were called "one day," not a "first
        day." Until the creation of other days, there was no series of days
        of which one could be called the first.
    </p>
</div>

<div class="footnote">
    <p id="_38">
        <sup>38</sup>: 
        See Jacob Schochet, <strong>Mystical Concepts in Chassidism</strong>, Kehot
        Publication Society, 1988, chapter 7.
    </p>
</div>

<div class="footnote">
    <p id="_39">
        <sup>39</sup>: 
        Idem, chapter 11.
    </p>
</div>

<div class="footnote">
    <p id="_40">
        <sup>40</sup>: 
        Earth is a mostly blue sphere surrounded by a vast expanse of
        empty space.
    </p>
</div>

<div class="footnote">
    <p id="_41">
        <sup>41</sup>: 
        A fundamental principle of quantum mechanics is that a photon
        behaves like a particle or a like a wave, depending on how it is
        observed.
    </p>
</div>

<div class="footnote">
    <p id="_42">
        <sup>42</sup>: 
        The interference of light waves can encode information, as in
        holography.
    </p>
</div>

<div class="footnote">
    <p id="_43">
        <sup>43</sup>: 
        See Isaiah 6:3.
    </p>
</div>

<div class="footnote">
    <p id="_44">
        <sup>44</sup>: 
        The divine reply reaches the interface of life and lifeless
        space.
    </p>
</div>

<div class="footnote">
    <p id="_45">
        <sup>45</sup>: 
        See Genesis 1:9. The dry land appeared as a result of the
        gathering together of the waters of the sea, thereby also creating
        waves on the shores of dry land.
    </p>
</div>

<div class="footnote">
    <p id="_46">
        <sup>46</sup>: 
        Biologists believe that terrestrial life evolved from sea
        creatures in littoral zones.
    </p>
</div>

<div class="footnote">
    <p id="_47">
        <sup>47</sup>: 
        Human fecundation too lies hidden under blankets.
    </p>
</div>

<div class="footnote">
    <p id="_48">
        <sup>48</sup>: 
        This poem describes the Ice Age.
    </p>
</div>

<div class="footnote">
    <p id="_49">
        <sup>49</sup>: 
        The primary source of oxygen in the atmosphere is plankton.
    </p>
</div>

<div class="footnote">
    <p id="_50">
        <sup>50</sup>: 
        See Talmud Bavli <strong>Bava Basra</strong> 75a. "The Holy One, blessed is He,
        will one day make a meal for the righteous from the flesh of the
        Leviathan."
    </p>
</div>

<div class="footnote">
    <p id="_51">
        <sup>51</sup>: 
        Since God's image is not a physical appearance, it could have
        been granted to a creature of the sea or sky.
    </p>
</div>

<div class="footnote">
    <p id="_52">
        <sup>52</sup>: 
        See Psalms 18:3.
    </p>
</div>

<div class="footnote">
    <p id="_53">
        <sup>53</sup>: 
        See Rashi on Genesis 2:7. "God collected man's soil from all the
        earth, from the four directions, so that anywhere man may die, there
        the earth will take him in for burial."
    </p>
</div>

<div class="footnote">
    <p id="_54">
        <sup>54</sup>: 
        Adam was created at the site where the Temple would be built. See
        Maimonides, <strong>Mishneh Torah</strong>, 
        <strong>Hilchos Bais HaBechirah</strong>, 2:2. "Adam,
        the first man, offered a sacrifice there and was created at that
        very spot, as our Sages said, 'Man was created from the place where
        he would find atonement.'" See also Jerusalem Talmud (hereinafter
        referred to as Yerushalmi), <strong>Nazir</strong> 7:2.
    </p>
</div>

<div class="footnote">
    <p id="_55">
        <sup>55</sup>: 
        See Talmud Bavli <strong>Eruvin</strong> 100b. "Had not the Torah been given, we
        would have learned modesty from a cat, not to commit theft from an
        ant, not to commit adultery from a dove, and the proper manner of
        conduct for marital relations from a rooster, which first appeases
        its mate and then has relations with it."
    </p>
</div>

<div class="footnote">
    <p id="_56">
        <sup>56</sup>: 
        The Hebrew word for blessing, <strong>berachah</strong>, is etymologically
        related to the Hebrew word for knee, <strong>barach</strong>.
    </p>
</div>

<div class="footnote">
    <p id="_57">
        <sup>57</sup>: 
        See Genesis 11:4-9.
    </p>
</div>

<div class="footnote">
    <p id="_58">
        <sup>58</sup>: 
        See Genesis 3:7.
    </p>
</div>

<div class="footnote">
    <p id="_59">
        <sup>59</sup>: 
        The expression "good and evil" is a merism. Before the First Sin,
        Adam and Eve could easily distinguish between good and evil. After
        it, they saw deeds as a mixture of the two.
    </p>
</div>

<div class="footnote">
    <p id="_60">
        <sup>60</sup>: 
        See *Hamlet*, Act II Scene 2. "The paragon of animals!"
    </p>
</div>

<div class="footnote">
    <p id="_61">
        <sup>61</sup>: 
        See Zohar III, 7b. "The union of male and female is termed
        'one.' ... For a male without a female is called 'half a body,'
        and a half is not 'one.' When the two halves unite, they then
        become one body, and they are then called 'one.'" See also Plato's
        <strong>Symposium</strong>.
    </p>
</div>

<div class="footnote">
    <p id="_62">
        <sup>62</sup>: 
        An homage to the first line of Milton's *Paradise Lost*.
    </p>
</div>

<div class="footnote">
    <p id="_63">
        <sup>63</sup>: 
        See Talmud Bavli <strong>Yoma</strong> 9b. "The Second Temple...was destroyed
        because of the gratuitous hatred that existed there."
    </p>
</div>

<div class="footnote">
    <p id="_64">
        <sup>64</sup>: 
        See Genesis 3:23.
    </p>
</div>

<div class="footnote">
    <p id="_65">
        <sup>65</sup>: 
        "Weak days" is a homonym of "week days."
    </p>
</div>

<div class="footnote">
    <p id="_66">
        <sup>66</sup>: 
        See Isaiah 58:12.
    </p>
</div>

<div class="footnote">
    <p id="_67">
        <sup>67</sup>: 
        Saul greets Samuel after the capture of Agag. See I Samuel 15:13.
    </p>
</div>

<div class="footnote">
    <p id="_68">
        <sup>68</sup>: 
        Samuel replies to Saul with rebuke. See I Samuel 15:14.
    </p>
</div>

<div class="footnote">
    <p id="_69">
        <sup>69</sup>: 
        Reddle (sometimes called ruddle) is a powder made of red ochre.
        It is used to mark sheep. Cf. the extensive use of reddle in *Return
        of the Native*.
    </p>
</div>

<div class="footnote">
    <p id="_70">
        <sup>70</sup>: 
        See I Samuel 15:32.
    </p>
</div>

<div class="footnote">
    <p id="_71">
        <sup>71</sup>: 
        See I Samuel 15:24.
    </p>
</div>

<div class="footnote">
    <p id="_72">
        <sup>72</sup>: 
        During the time between his capture and execution, Agag sired a
        son. See Talmud Bavli <strong>Megillah</strong> 13a. "...for Saul did not kill
        Agag, from whom descended Haman, who oppresses the Jews." See also
        Esther 3:1.
    </p>
</div>

<div class="footnote">
    <p id="_73">
        <sup>73</sup>: 
        See I Samuel 15:27.
    </p>
</div>

<div class="footnote">
    <p id="_74">
        <sup>74</sup>: 
        See I Samuel 15:17. See also I Samuel 9:3.
    </p>
</div>

<div class="footnote">
    <p id="_75">
        <sup>75</sup>: 
        See I Samuel 16:19-23.
    </p>
</div>

<div class="footnote">
    <p id="_76">
        <sup>76</sup>: 
        Saul addresses Jonathan.
    </p>
</div>

<div class="footnote">
    <p id="_77">
        <sup>77</sup>: 
        See I Samuel 20:30-31.
    </p>
</div>

<div class="footnote">
    <p id="_78">
        <sup>78</sup>: 
        See I Samuel 18:4.
    </p>
</div>

<div class="footnote">
    <p id="_79">
        <sup>79</sup>: 
        The name Jonathan means "God has given."
    </p>
</div>

<div class="footnote">
    <p id="_80">
        <sup>80</sup>: 
        In his melancholy, Saul addresses his court. See I Samuel 16:14.
    </p>
</div>

<div class="footnote">
    <p id="_81">
        <sup>81</sup>: 
        See I Samuel 18:7.
    </p>
</div>

<div class="footnote">
    <p id="_82">
        <sup>82</sup>: 
        The Philistines worshipped a fish-god named Dagon. See I Samuel
        5:1-5, and Rashi op.cit.
    </p>
</div>

<div class="footnote">
    <p id="_83">
        <sup>83</sup>: 
        Saul addresses his armor-bearer.
    </p>
</div>

<div class="footnote">
    <p id="_84">
        <sup>84</sup>: 
        Mount Gilboa, where Saul died, is the home of a purple Iris
        called the Gilboa Iris.
    </p>
</div>

<div class="footnote">
    <p id="_85">
        <sup>85</sup>: 
        Only Saul's bones, not his whole body, were buried. See I Samuel
        31:12-13.
    </p>
</div>

<div class="footnote">
    <p id="_86">
        <sup>86</sup>: 
        See I Samuel 15:27.
    </p>
</div>

<div class="footnote">
    <p id="_87">
        <sup>87</sup>: 
        See I Samuel 31:4.
    </p>
</div>

<div class="footnote">
    <p id="_88">
        <sup>88</sup>: 
        Michal's thought are indicated by italics.
    </p>
</div>

<div class="footnote">
    <p id="_89">
        <sup>89</sup>: 
        Michal addresses David. See II Samuel 6:16.
    </p>
</div>

<div class="footnote">
    <p id="_90">
        <sup>90</sup>: 
        See I Samuel 25:22. The phrase משתין בקיר is sometimes translated
        as "a dog," but literally means one who pisses against a wall.
    </p>
</div>

<div class="footnote">
    <p id="_91">
        <sup>91</sup>: 
        Saul was unusually tall. See I Samuel 9:2.
    </p>
</div>

<div class="footnote">
    <p id="_92">
        <sup>92</sup>: 
        See I Samuel 24:5.
    </p>
</div>

<div class="footnote">
    <p id="_93">
        <sup>93</sup>: 
        See I Kings 1:1. See also Talmud Bavli <strong>Berachos</strong> 62b: "Whoever
        dishonors clothing will in the end not have use of them."
    </p>
</div>

<div class="footnote">
    <p id="_94">
        <sup>94</sup>: 
        David addresses Bathsheba.
    </p>
</div>

<div class="footnote">
    <p id="_95">
        <sup>95</sup>: 
        See II Samuel 11:1-5.
    </p>
</div>

<div class="footnote">
    <p id="_96">
        <sup>96</sup>: 
        See Jonah 2:6-7.
    </p>
</div>

<div class="footnote">
    <p id="_97">
        <sup>97</sup>: 
        See Psalms 118:5.
    </p>
</div>

<div class="footnote">
    <p id="_98">
        <sup>98</sup>: 
        See Psalms 51:5.
    </p>
</div>

<div class="footnote">
    <p id="_99">
        <sup>99</sup>: 
        Nathan addresses David.
    </p>
</div>

<div class="footnote">
    <p id="_100">
        <sup>100</sup>: 
        See II Samuel 12:3.
    </p>
</div>

<div class="footnote">
    <p id="_101">
        <sup>101</sup>: 
        See II Samuel 12:5-6. See also Exodus 21:37.
    </p>
</div>

<div class="footnote">
    <p id="_102">
        <sup>102</sup>: 
        See II Samuel 12:7.
    </p>
</div>

<div class="footnote">
    <p id="_103">
        <sup>103</sup>: 
        See Talmud Bavli <strong>Yoma</strong> 12b. In fact, four were indeed taken
        from among David's offsprings: Bathsheba's first-born child, who
        died in infancy (II Samuel 2:18); Tamar, who was raped (II Samuel
        13:14); Amnon, who was murdered in revenge by his half-brother (II
        Samuel 13:29); and Absalom, who died in a war of rebellion against
        his father (II Samuel 18:14-15).
    </p>
</div>

<div class="footnote">
    <p id="_104">
        <sup>104</sup>: 
        See II Samuel 12:10.
    </p>
</div>

<div class="footnote">
    <p id="_105">
        <sup>105</sup>: 
        David addresses the dead Absalom.
    </p>
</div>

<div class="footnote">
    <p id="_106">
        <sup>106</sup>: 
        See II Samuel 19:1-5.
    </p>
</div>

<div class="footnote">
    <p id="_107">
        <sup>107</sup>:
        In Israel, the prickly pear is known as the <strong>sabra</strong>. Native-born
        Israelis have adopted the name for themselves, since the fruit is
        prickly on the outside, but sweet on the inside. The sweet pulp of
        the fruit is orange color.
    </p>
</div>

<div class="footnote">
    <p id="_108">
        <sup>108</sup>: 
        Absalom had remarkably abundant hair. See II Samuel 14:26.
    </p>
</div>

<div class="footnote">
    <p id="_109">
        <sup>109</sup>: 
        According to Talmud Bavli <strong>Sotah</strong> 10b, David uttered the word(s)
        "my son" eight times—seven times to raise Absalom from the seven
        chambers of Gehinnom (the underworld), and one more time to elevate
        him to the World to Come.
    </p>
</div>

<div class="footnote">
    <p id="_110">
        <sup>110</sup>: 
        David addresses Abishag the Shunammite.
    </p>
</div>

<div class="footnote">
    <p id="_111">
        <sup>111</sup>: 
        See I Kings 1:1-4.
    </p>
</div>

<div class="footnote">
    <p id="_112">
        <sup>112</sup>: 
        David's mind begins to drift as he lay dying.
    </p>
</div>

<div class="footnote">
    <p id="_113">
        <sup>113</sup>: 
        See I Samuel 17:4.
    </p>
</div>

<div class="footnote">
    <p id="_114">
        <sup>114</sup>: 
        See I Samuel 17:51.
    </p>
</div>

<div class="footnote">
    <p id="_115">
        <sup>115</sup>: 
        See II Samuel 12:18.
    </p>
</div>

<div class="footnote">
    <p id="_116">
        <sup>116</sup>: 
        See II Samuel 12:23.
    </p>
</div>

<div class="footnote">
    <p id="_117">
        <sup>117</sup>: 
        David experiences terminal hallucinations. See II Samuel 6:20.
    </p>
</div>

<div class="footnote">
    <p id="_118">
        <sup>118</sup>: 
        See I Samuel 20:41-42.
    </p>
</div>

<div class="footnote">
    <p id="_119">
        <sup>119</sup>: 
        Bathsheba's inner thoughts are in italics.
    </p>
</div>

<div class="footnote">
    <p id="_120">
        <sup>120</sup>: 
        See Psalms 51:15. Bathsheba considers the verse quite
        differently from the way David did.
    </p>
</div>

<div class="footnote">
    <p id="_121">
        <sup>121</sup>: 
        The name Bathsheba means "seven year old girl" in Hebrew.
    </p>
</div>

<div class="footnote">
    <p id="_122">
        <sup>122</sup>: 
        See I Kings 1:17.
    </p>
</div>

<div class="footnote">
    <p id="_123">
        <sup>123</sup>: 
        Brittle hair, cold intolerance and mental clouding are symptoms
        of end-stage hypothyroidism, a possible cause of David's death.
    </p>
</div>

<div class="footnote">
    <p id="_124">
        <sup>124</sup>: 
        In the Bronze Age, some royalty were buried in cedar coffins.
    </p>
</div>

<div class="footnote">
    <p id="_125">
        <sup>125</sup>: 
        Solomon's Temple was lined with cedar wood. See I Kings 6:15.
    </p>
</div>

<div class="footnote">
    <p id="_126">
        <sup>126</sup>: 
        Joab had supported a rebellion against Solomon. See I Kings
        2:28.
    </p>
</div>

<div class="footnote">
    <p id="_127">
        <sup>127</sup>: 
        Benaiah addresses Solomon.
    </p>
</div>

<div class="footnote">
    <p id="_128">
        <sup>128</sup>: 
        See I Kings 2:30.
    </p>
</div>

<div class="footnote">
    <p id="_129">
        <sup>129</sup>: 
        Joab here quotes David. See II Samuel 11:25.
    </p>
</div>

<div class="footnote">
    <p id="_130">
        <sup>130</sup>: 
        When Joab killed Abner, David cursed him concerning his progeny.
        (See II Samuel 3:29.) In confronting Benaiah, Joab argued his
        execution at Solomon's command would represent double punishment for
        one crime. Therefore, the curses which David had put on him would be
        transferred to Solomon. So it was. See Talmud Bavli *Sanhedrin* 48b
        for details.
    </p>
</div>

<div class="footnote">
    <p id="_131">
        <sup>131</sup>: 
        Solomon address his friend Zabud. See I Kings 4:5.
    </p>
</div>

<div class="footnote">
    <p id="_132">
        <sup>132</sup>: 
        The two women present their case differently. The plaintiff
        first discusses the dead child, and the defendant first discusses
        the live child. Each is describing her own child.
    </p>
</div>

<div class="footnote">
    <p id="_133">
        <sup>133</sup>: 
        Solomon knew which was the real mother before he ordered that a
        sword be brought to cut the child in half. But if he simply made a
        ruling, the court might think that the decision was arbitrary or
        incorrect.
    </p>
</div>

<div class="footnote">
    <p id="_134">
        <sup>134</sup>: 
        Solomon recognized that the two women were living together
        without a man because they were mother- and daughter-in-law, both of
        whose husbands had recently died. The daughter-in-law had a motive
        to lie, because then she would appear to be relieved of having to
        wait until the boy were 13 years old to relieve her of the duty of
        levirate marriage (*yibum*). All the more so she had a motive to see
        the child killed by the king, because then in actuality, not merely
        appearance, she would be released from a levirate marriage. It is
        ordinary insight to recognize a woman who would have the baby killed
        as not being the real mother. But Solomon knew that the false
        claimant would not simply say, "See! She says give the baby to me.
        She is willing to kidnap but not to murder!" His wisdom lay in his
        knowing that she would agree to having the baby killed, not merely
        in recognizing that her saying so identified her as the false
        claimant.
    </p>
</div>

<div class="footnote">
    <p id="_135">
        <sup>135</sup>: 
        See I Kings 3:5-9.
    </p>
</div>

<div class="footnote">
    <p id="_136">
        <sup>136</sup>: 
        See Proverbs 1:8.
    </p>
</div>

<div class="footnote">
    <p id="_137">
        <sup>137</sup>: 
        Solomon addresses God.
    </p>
</div>

<div class="footnote">
    <p id="_138">
        <sup>138</sup>: 
        See Ecclesiastes 1:9.
    </p>
</div>

<div class="footnote">
    <p id="_139">
        <sup>139</sup>: 
        See Ecclesiastes 2:1-11.
    </p>
</div>

<div class="footnote">
    <p id="_140">
        <sup>140</sup>: 
        See Ecclesiastes 2:14.
    </p>
</div>

<div class="footnote">
    <p id="_141">
        <sup>141</sup>: 
        See Ecclesiastes 6:1-2.
    </p>
</div>

<div class="footnote">
    <p id="_142">
        <sup>142</sup>: 
        See Ecclesiastes 3:19-21.
    </p>
</div>

<div class="footnote">
    <p id="_143">
        <sup>143</sup>: 
        See Ecclesiastes 4:2.
    </p>
</div>

<div class="footnote">
    <p id="_144">
        <sup>144</sup>: 
        Solomon addresses his army.
    </p>
</div>

<div class="footnote">
    <p id="_145">
        <sup>145</sup>: 
        See I Kings 5:5.
    </p>
</div>

<div class="footnote">
    <p id="_146">
        <sup>146</sup>: 
        See Joshua 1:4.
    </p>
</div>

<div class="footnote">
    <p id="_147">
        <sup>147</sup>: 
        The inability to anticipate pleasure is a hallmark of
        depression.
    </p>
</div>

<div class="footnote">
    <p id="_148">
        <sup>148</sup>: 
        See Job 1:1-3.
    </p>
</div>

<div class="footnote">
    <p id="_149">
        <sup>149</sup>: 
        See Job 29:12-16.
    </p>
</div>

<div class="footnote">
    <p id="_150">
        <sup>150</sup>: 
        See Job 10:1-7.
    </p>
</div>

<div class="footnote">
    <p id="_151">
        <sup>151</sup>: 
        See Job 1:18-19.
    </p>
</div>

<div class="footnote">
    <p id="_152">
        <sup>152</sup>: 
        See Job 1:11-14.
    </p>
</div>

<div class="footnote">
    <p id="_153">
        <sup>153</sup>: 
        Job questions whether there is more to man than his body.
    </p>
</div>

<div class="footnote">
    <p id="_154">
        <sup>154</sup>: 
        See Job 2:8. An ostracon is synonymous with a potsherd. Ostraca
        were used in Greek society as a tool for ostracizing members from
        society.
    </p>
</div>

<div class="footnote">
    <p id="_155">
        <sup>155</sup>: 
        Many skin diseases, and impetigo in particular, are
        characterized by honey-colored scabs fromed from dried serum.
    </p>
</div>

<div class="footnote">
    <p id="_156">
        <sup>156</sup>: 
        See Job 3:22. See also "Eclipse of God," by Martin Buber.
    </p>
</div>

<div class="footnote">
    <p id="_157">
        <sup>157</sup>: 
        Job's wife, too, is childless.
    </p>
</div>

<div class="footnote">
    <p id="_158">
        <sup>158</sup>: 
        See Job 2:9.
    </p>
</div>

<div class="footnote">
    <p id="_159">
        <sup>159</sup>: 
        The lentils of the near middle east are orange in color. Lentils
        are a traditional meal of mourning.
    </p>
</div>

<div class="footnote">
    <p id="_160">
        <sup>160</sup>: 
        See Job 4-6. Job's first comforter, Eliphaz, argues that the
        world is justly run according to divinely established natural laws.
    </p>
</div>

<div class="footnote">
    <p id="_161">
        <sup>161</sup>: 
        See Job 6-7. Job responds to Eliphaz, saying that while the
        world may be orderly, it lacks justice.
    </p>
</div>

<div class="footnote">
    <p id="_162">
        <sup>162</sup>: 
        See Job 8. Job's second comforter, Bildad, argues that there is
        in fact justice in the world, implying that Job may not be as
        innocent as he claims.
    </p>
</div>

<div class="footnote">
    <p id="_163">
        <sup>163</sup>: 
        See Job 3:10.
    </p>
</div>

<div class="footnote">
    <p id="_164">
        <sup>164</sup>: 
        Job replies that there is no justice for *him*.
    </p>
</div>

<div class="footnote">
    <p id="_165">
        <sup>165</sup>: 
        See Job 11. Job's third comforter, Zophar, directly challenges
        Job's claim of innocence, and tells him that his misery is a just
        punishment for Job's wickedness.
    </p>
</div>

<div class="footnote">
    <p id="_166">
        <sup>166</sup>: 
        Confession precedes repentance. See Proverbs 28:13.
    </p>
</div>

<div class="footnote">
    <p id="_167">
        <sup>167</sup>: 
        See Job 13:17-23. Job continues to plead his innocence and
        demands that God respond to his testimony.
    </p>
</div>

<div class="footnote">
    <p id="_168">
        <sup>168</sup>: 
        God answers Job. See Job 38-39. The Creator is characterized by
        unity; creation is characterized by duality (such as wave-particle
        duality).
    </p>
</div>

<div class="footnote">
    <p id="_169">
        <sup>169</sup>: 
        Nachmanides, in *Toras Hashem Temima* writes that God created
        the world from absolute nothingness. "The thing that was created
        was a small object that was as small as a mustard seed; this was the
        heavens and everything in them." This description is similar to
        that of the standard model Big Bang theory.
    </p>
</div>

<div class="footnote">
    <p id="_170">
        <sup>170</sup>: 
        When the foot is pricked by a pin, the leg pulls away even
        before pain is perceived.
    </p>
</div>

<div class="footnote">
    <p id="_171">
        <sup>171</sup>: 
        Cf. Immanuel Kant, Critique of Practical Reason: "Two things
        fill the mind with ever-increasing wonder and awe, the more often
        and the more intensely the mind of thought is drawn to them: the
        starry heavens above me and the moral law within me."
    </p>
</div>

<div class="footnote">
    <p id="_172">
        <sup>172</sup>: 
        The essence of space is that it distinguishes, that is, it
        separates otherwise identical simultaneous events.
    </p>
</div>

<div class="footnote">
    <p id="_173">
        <sup>173</sup>: 
        One who has all he wants cannot desire.
    </p>
</div>
