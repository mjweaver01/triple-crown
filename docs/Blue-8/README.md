---
title: God’s Blue Throne
meta:
    - name: description
      content: Triple Crown - God’s Blue Throne
    - name: keywords
      content: Triple Crown - God’s Blue Throne
---

# God’s Blue Throne

![](./media/image9.png)

Up to us, the sparks to reunite

Into a realm of holiness on earth;

To raise, redeem, the scattered shards of light

Inherent, hidden in apparent dearth;

To gather them and us upon a shore

Whose edge of blue abuts a sunless sea;<sup>[1](#_1)</sup>

And send them soaring homeward; to restore

**T**o each the place where it was meant to be.

Each particle of light, a wave is too,<sup>[2](#_2)</sup>

And interferes with others in its course.<sup>[3](#_3)</sup>

To whom send such a message? Replies who?

Detected and reflected by what force?

And yet—like angels calling each to each,<sup>[4](#_4)</sup>

<p><router-link to="/blue-9">Evoked by love, the waves caress the beach.</router-link><sup><a href="#_5">5</a></sup></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        Earth is a mostly blue sphere surrounded by a vast expanse of
        empty space.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        A fundamental principle of quantum mechanics is that a photon
        behaves like a particle or a like a wave, depending on how it is
        observed.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        The interference of light waves can encode information, as in
        holography.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        See Isaiah 6:3.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        The divine reply reaches the interface of life and lifeless
        space.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="blue" to="/blue-7">Previous Poem</router-link>
  <router-link class="blue" to="/blue-9">Next Poem</router-link>
</div>

<br>

<AmazonLink />