---
title: God’s Blue Throne
meta:
    - name: description
      content: Triple Crown - God’s Blue Throne
    - name: keywords
      content: Triple Crown - God’s Blue Throne
---

# God’s Blue Throne

![](./media/image6.png)

Safeguard them nigh, and me, within Your reach.

Safeguard these sparks primaeval, holy sparks<sup>[1](#_1)</sup>

Suffusing all the world, a world in each,

All top and bottom strange and charming quarks.<sup>[2](#_2)</sup>

Safeguard the scintillation<sup>[3](#_3)</sup> tinted green<sup>[4](#_4)</sup>

Which spans between creation and the pit.

Safeguard the holy glue<sup>[5](#_5)</sup> that binds unseen

**H**adrons,<sup>[6](#_6)</sup> heaven—songs done<sup>[7](#_7)</sup> and songs unwrit.

Safeguard us too as we fulfill the task

To elevate the sparks from matter coarse,

To blow on glowing embers, and to bask

In light reborn returning to its source.

Safeguard us. We repair a wall first breached<sup>[8](#_8)</sup>

<p><router-link to="/blue-6">Before the force of nature was unleashed.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        It is a basic tenet of Lurianic Kabbalah that in the pre-temporal
        era of creation, God sent forth ten vessels of holy light, but the
        vessels shattered, scattering their sparks throughout the mundane
        world. The Ari (Rabbi Isaac Luria, 1534-1572) taught: "There is no
        sphere of existence, including organic and inorganic nature, that is
        not full of holy sparks which are mixed in with the <i>kelippot</i>
        [husks], and need to be separated from them and lifted up."
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        Quarks are elementary particles and a fundamental constituent of
        matter. There are six types of quarks, known as flavors: up, down,
        strange, charm, top, and bottom.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        The process of scintillation is one of luminescence whereby light
        of a characteristic spectrum is emitted following the absorption of
        radiation.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        The green Earth exists as a temporal and spatial oasis between
        Creation and the final collapse of the solar system.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        Quarks are bound together by elementary particles called gluons
        to form neutrons or protons.
    </p>
</div>

<div class="footnote">
    <p id="_6">
        <sup>6</sup>: 
        Hadrons are elementary particles comprised of quarks and other
        elementary particles.
    </p>
</div>

<div class="footnote">
    <p id="_7">
        <sup>7</sup>: 
        An *homage* to the poetry of John Donne.
    </p>
</div>

<div class="footnote">
    <p id="_8">
        <sup>8</sup>: 
        See Isaiah 58:12. "Ancient ruins will be rebuilt through you, and
        you will restore generations-old foundation; and they will call you
        'repairer of the breach' and 'restorer of paths for habitation.'"
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="blue" to="/blue-4">Previous Poem</router-link>
  <router-link class="blue" to="/blue-6">Next Poem</router-link>
</div>

<br>

<AmazonLink />