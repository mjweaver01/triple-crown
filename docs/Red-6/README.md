---
title: Oh Zealots So Red
meta:
    - name: description
      content: Triple Crown - Oh Zealots So Red
    - name: keywords
      content: Triple Crown - Oh Zealots So Red
---

# Oh Zealots So Red

![](./media/image21.png)

*As paradise regained swells into reach,*<sup>[1](#_1)</sup>

*He rouses* *horded treasure of my scent.*

*The seed that lies within a ruby peach,*

*So was my hero husband in my tent.*

But now you hike your tunic,<sup>[2](#_2)</sup> raise your leg

Like mongrel dogs that piss against a wall.<sup>[3](#_3)</sup>

My daddy earned respect you cannot beg,

**N**or ever did he stoop to be so small.<sup>[4](#_4)</sup>

You were not fit to even touch his hem.

How dare you snip his cloak<sup>[5](#_5)</sup>—a king like him!

Your beddings, cold, deny you and condemn.<sup>[6](#_6)</sup>

*He was the bridegroom in my chamber dim,*

*And I the virgin princess clad in white.*

<p><router-link to="/red-7"><i>"Let there be light," He said. And there was light.</i></router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        Michal's thought are indicated by italics.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        Michal addresses David. See II Samuel 6:16.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        See I Samuel 25:22. The phrase משתין בקיר is sometimes translated
        as "a dog," but literally means one who pisses against a wall.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        Saul was unusually tall. See I Samuel 9:2.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        See I Samuel 24:5.
    </p>
</div>

<div class="footnote">
    <p id="_6">
        <sup>6</sup>: 
        See I Kings 1:1. See also Talmud Bavli <i>Berachos</i> 62b: "Whoever
        dishonors clothing will in the end not have use of them."
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="red" to="/red-5">Previous Poem</router-link>
  <router-link class="red" to="/red-7">Next Poem</router-link>
</div>

<br>

<AmazonLink />