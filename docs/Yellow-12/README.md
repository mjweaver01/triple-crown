---
title: Dust—All’s Yellow
meta:
    - name: description
      content: Triple Crown - Dust—All’s Yellow
    - name: keywords
      content: Triple Crown - Dust—All’s Yellow
---

# Dust—All’s Yellow

![](./media/image38.png)

"Lament some other human tragedy,"

Replies my final friend.  "No more assault

Our ears with self-indulgent threnody

For losses which, in truth, are your own fault.<sup>[1](#_1)</sup>

You did not rail against divine decree

While basking in your health and wealth and name;

Now just conviction makes you disagree.

**V**ituperate yourself, for you're to blame.

You planted vetch, and now would harvest grape,

Sinned secretly, now innocence declaim.

The honey-crusted oozings that you scrape

Make evident your guilt, if not your shame.

Confess. Repent.<sup>[2](#_2)</sup> No longer desecrate,

<p><router-link to="/yellow-13">Lest man believe that he is thrall to fate."</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        See Job 11. Job's third comforter, Zophar, directly challenges
        Job's claim of innocence, and tells him that his misery is a just
        punishment for Job's wickedness.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        Confession precedes repentance. See Proverbs 28:13.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="yellow" to="/yellow-11">Previous Poem</router-link>
  <router-link class="yellow" to="/yellow-13">Next Poem</router-link>
</div>

<br>

<AmazonLink />
