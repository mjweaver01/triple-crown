---
title: God’s Blue Throne
meta:
    - name: description
      content: Triple Crown - God’s Blue Throne
    - name: keywords
      content: Triple Crown - God’s Blue Throne
---

# God’s Blue Throne

![](./media/image10.png)

Evoked by love, the waves caress the beach.<sup>[1](#_1)</sup>

The beach, in turn, yields grain by grain her sand,

Like lovers wrapped in rapture, each in each:

The ageless love of water and of land.

In tidal pools, in basins, in the muds

Of swamps and marshes, prairies soaked with rain,

In gullies gouged by angry ancient floods,

**H**ere life may start, and end, and start again.<sup>[2](#_2)</sup>

Beneath the frost-tipped lilacs, earth is rife

With musty germinations. The untold

And ceaseless, restless writhing of new life

That stirs amid the rank decay of old

Lies hidden, fecundation no one sees:<sup>[3](#_3)</sup>

<p><router-link to="/blue-10">The land is blanketed with grass and trees.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        See Genesis 1:9. The dry land appeared as a result of the
        gathering together of the waters of the sea, thereby also creating
        waves on the shores of dry land.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        Biologists believe that terrestrial life evolved from sea
        creatures in littoral zones.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        Human fecundation too lies hidden under blankets.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="blue" to="/blue-8">Previous Poem</router-link>
  <router-link class="blue" to="/blue-10">Next Poem</router-link>
</div>

<br>

<AmazonLink />