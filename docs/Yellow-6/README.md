---
title: Dust—All’s Yellow
meta:
    - name: description
      content: Triple Crown - Dust—All’s Yellow
    - name: keywords
      content: Triple Crown - Dust—All’s Yellow
---

# Dust—All’s Yellow

![](./media/image34.png)

As muzzy-mumble sages nod and teach

Of worlds they've never seen, they promise there

Is found the world of truth.  And as they preach,

I scrape my skin in loathing and despair.

Here is the truth: the yellow scum I scrape

From stinking flesh.  Here is the truth:  the loss

Of all I was, the truth none can escape,

**R**eality revealed in all its dross.

The death of hope illuminates the world,

Allows no further loss, no more deceit,

No blessings, curses, imprecations hurled,

No more illusions ceaseless to repeat.

The truth is revelation blinding bright.

<p><router-link to="/yellow-7">"Let there be light," He said, and there was light.</router-link></p>

<br>

<div class="prev-next">
  <router-link class="yellow" to="/yellow-5">Previous Poem</router-link>
  <router-link class="yellow" to="/yellow-7">Next Poem</router-link>
</div>

<br>

<AmazonLink />