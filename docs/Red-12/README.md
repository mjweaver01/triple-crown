---
title: Oh Zealots So Red
meta:
    - name: description
      content: Triple Crown - Oh Zealots So Red
    - name: keywords
      content: Triple Crown - Oh Zealots So Red
---

# Oh Zealots So Red

![](./media/image26.png)

Of cedar paneled chambers,<sup>[1](#_1)</sup> and the vow 

That Joab swore, and how your foe did cling

In vain to our most holy shrine,<sup>[2](#_2)</sup> I'll now

Relate, my lord—and may your servant bring

A smile of pleasure to your face.<sup>[3](#_3)</sup>  He swore

He'd never leave that place—and it was so.<sup>[4](#_4)</sup>

Defile the altar with his very gore—

**T**hat was his plan—and with that same blood flow

To stain your name.  He mocked you, said the sword

Devours this way and that,<sup>[5](#_5)</sup> and now, withdrawn

From him, his curse inures to you, my lord:

Impure and leprous, lame and hungry spawn.<sup>[6](#_6)</sup>

I struck—and summoned him as he lay dead,

<p><router-link to="/red-13">"Repeat it <i>now</i>—exactly as you said."</router-link>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        Solomon's Temple was lined with cedar wood. See I Kings 6:15.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        Joab had supported a rebellion against Solomon. See I Kings
        2:28.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        Benaiah addresses Solomon.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        See I Kings 2:30.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        Joab here quotes David. See II Samuel 11:25.
    </p>
</div>

<div class="footnote">
    <p id="_6">
        <sup>6</sup>: 
        When Joab killed Abner, David cursed him concerning his progeny.
        (See II Samuel 3:29.) In confronting Benaiah, Joab argued his
        execution at Solomon's command would represent double punishment for
        one crime. Therefore, the curses which David had put on him would be
        transferred to Solomon. So it was. See Talmud Bavli <i>Sanhedrin</i> 48b
        for details.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="red" to="/red-11">Previous Poem</router-link>
  <router-link class="red" to="/red-13">Next Poem</router-link>
</div>

<br>

<AmazonLink />