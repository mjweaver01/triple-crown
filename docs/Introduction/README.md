---
title: Introduction
pageClass: introduction
meta:
    - name: description
      content: Triple Crown - Introduction
    - name: keywords
      content: Triple Crown - Introduction
---

# Introduction

*Triple Crown* is, to my knowledge, the only triple heroic crown of
sonnets in the English language. I believe it is the only such poetic
work in any language. Therefore, some words of introduction are
certainly appropriate, probably helpful, and possibly necessary.<sup>[1](#_1)</sup>

A crown of sonnets is a series of sonnets linked one to another, such
that the last line of one poem is repeated as the first line of the
subsequent poem. This process continues until the loop is closed by the
last line of the last poem's being repeated as the first line of the
first poem. A classic example of this is the exquisite seven poem crown
of sonnets by John Donne entitled, aptly enough, *La Corona.*

A heroic crown of sonnets is a sonnet crown consisting of fourteen poems
linked as described above. The added feature which makes the heroic
crown more than simply a longer collection of poems is that the linking
(repeated) lines of poetry themselves comprise another sonnet. Thus, a
fifteenth sonnet is implicit in the fourteen sonnets explicitly stated
in the heroic crown. This fifteenth sonnet is called the magistral
sonnet. As an added feature, the magistral sonnet is often an acrostic,
with the acronym embedded within it pithily summarizing the theme of the
entire set.

One can represent a heroic crown of sonnets, then, as a series of small
filled circles linked together by a larger circle, something like beads
on a necklace. It will be readily apparent that one could have three
such circles, with each circle intersecting the other two at two places.
One of the sites of intersection is common to all three circles. (See
figure below.) Each cycle of poetry, therefore, will contain three poems
whose first lines also serve as first lines in poems of the other two
cycles. This is the fundamental structure of *Triple Crown*.

![](./media/image1.png)

*Triple Crown* contains several other structural features. Naturally,
the magistral sonnet of each cycle still serves as an acrostic.
Additionally, the eighth line of each of the poems (excluding the
magistral sonnets) also form an acrostic spanning the entire poetic
work. In the text, the first letter of these lines is in bold font.

The three cycles of *Triple Crown* are not merely structured
thematically; they are also structured chromatically. Indeed, the
chromatic structure reflects the thematic structure.

Blue is the color ascribed to heaven and all that emanates from it. The
magistral sonnet of the blue cycle is a recapitulation of the story of
Creation, just as the poems within that cycle tell the story in greater
detail. Every poem of the blue cycle, except as noted below, refers to
some blue aspect of Creation.

Red represents conflict. The red cycle tells the stories of conflict in
the lives of the three great kings of Israel―Saul, David, and Solomon.
The magistral sonnet of that collection retells the story found in *Song
of Songs*; that great poem, too, tells of the conflict―love,
estrangement, and reconciliation―of God and the Jewish people. The poems
of the red cycle, again with the exception noted below, reveal the
redness of their subject.

Yellow is the color of dust, decline, and decay. The magistral sonnet of
the yellow cycle summarizes the story of Job, and the poems of that
cycle tell Job's story in greater detail. These poems, like those of the
other two cycles, reflect their thematic color, except as noted below.

The exception to the color coding of each cycle is the result of the
cycles intersecting. Where the blue and red cycles intersect, the
thematic color must be purple. Where the blue and yellow cycles
intersect, the thematic color must be green, and of course the
intersection of the red and yellow cycles is portrayed, however
implicitly, by the color orange. Where all three cycles intersect, the
colors combine to make white light. The poems at that unique
intersection are not sonnets, but villanelles.

Creation is known from afar. The blue cycle portraying Creation is
therefore written primarily in the third person. Conflict is dyadic. The
red cycle, therefore, is excerpts of dialogue, written primarily in the
second person. Suffering is personal. The yellow cycle is written in the
first person. There are, of course, many more structural and thematic
features in *Triple Crown*. I leave it to the reader to find or invent
these, and to enjoy them.

Above each poem in the book is a diagram indicating where, in the total
structure, the poem lies. This should help keep the reader apprised as
to his location within the entire work.

*Triple Crown* is not a collection of poems. It is a unified work. It is
intended to be read in the order presented. It merits re-reading.<sup>[2](#_2)</sup>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        The adverbs here decrease in intensity as the adjectives increase
        in intensity. A similar pattern is seen in Psalms 1:1. "The praises
        of Man are that he walked not in the counsel of the wicked, and
        stood not in the path of the sinful, and sat not in the session of
        scorners." Walking by gives less exposure than standing, and
        standing gives less exposure than sitting. The wicked are worse than
        the sinful, and the sinful are worse than the scorners.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See author's introduction to <i>The Path of the Just</i> (<i>Mesillat
        Yesharim</i>), by Rabbi Moshe Chayim Luzzatto.
    </p>
</div>

<br>

<div class="prev-next">
  <span></span>
  <router-link class="blue" to="/blue-1">God’s Blue Throne</router-link>
</div>

<br>

<AmazonLink />