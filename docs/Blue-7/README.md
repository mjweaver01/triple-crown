---
title: God’s Blue Throne
meta:
    - name: description
      content: Triple Crown - God’s Blue Throne
    - name: keywords
      content: Triple Crown - God’s Blue Throne
---

# God’s Blue Throne

![](./media/image8.png)

"Let there be light," He said, and there was light.<sup>[1](#_1)</sup>

The scattered sparks flew each its willful way.

(Up to us, the sparks to reunite.)

And there was evening, darkness renamed night,<sup>[2](#_2)</sup>

And there was morning on that unique day<sup>[3](#_3)</sup>

"Let there be light," He said, and there was light.

The spheres that held them shattered.<sup>[4](#_4)</sup>  Sparks in flight

**N**ow fled in hell-bent scattered disarray.

(Up to us, the sparks to reunite.)<sup>[5](#_5)</sup>

How glorious stretched eternity, and bright.

How pure and perfect past and future lay.

"Let there be light," He said, and there was light.

But pure and perfect bore within it blight;

Eternity proved mother to decay.

(Up to us, the sparks to reunite.)

Or is this greater glory, recondite,

A co-creating part for us to play?

"Let there be light," He said, and there was light.

<p><router-link to="/blue-8">(Up to us, the sparks to reunite.)</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        See Genesis 1:3.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See Genesis 1:5. Darkness existed before God named it "night."
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        Ibid. The evening and morning were called "one day," not a "first
        day." Until the creation of other days, there was no series of days
        of which one could be called the first.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        See Jacob Schochet, <i>Mystical Concepts in Chassidism</i>, Kehot
        Publication Society, 1988, chapter 7.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        Idem, chapter 11.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="blue" to="/blue-6">Previous Poem</router-link>
  <router-link class="blue" to="/blue-8">Next Poem</router-link>
</div>

<br>

<AmazonLink />