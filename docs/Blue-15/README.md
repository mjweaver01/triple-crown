---
title: God’s Blue Throne
meta:
    - name: description
      content: Triple Crown - God’s Blue Throne
    - name: keywords
      content: Triple Crown - God’s Blue Throne
---

# God’s Blue Throne

![](./media/image16.png)

Evermore and more, God grants us rest

From gleaning, 'mid the dust and broken clay,

The crumbs of daily crust which may, at best,

Suppress the pangs of hunger for the day.

Creation's last blue day, when we have done

The work that God created us to do

Reintegrates the broken parts to One

**E**ternal, unifying sparks anew.

O holy day beyond the mortal ken!

O day we struggle all our days to reach!

O day when we may sigh a last amen,

Restore at last the long neglected breach.<sup>[1](#_1)</sup>

No more to live in yearning, I beseech,

<p><router-link to="/blue-2">Grandiloquent, creating with just speech.</router-link></p>

### Footnote

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        See Isaiah 58:12.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="blue" to="/blue-14">Previous Poem</router-link>
  <router-link class="red" to="/red-1">Next Poem</router-link>
</div>

<br>

<AmazonLink />