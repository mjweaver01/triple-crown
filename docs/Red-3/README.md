---
title: Oh Zealots So Red
meta:
    - name: description
      content: Triple Crown - Oh Zealots So Red
    - name: keywords
      content: Triple Crown - Oh Zealots So Red
---

# Oh Zealots So Red

![](./media/image19.png)

Hear songs of love, and poetry recite—<sup>[1](#_1)</sup>

And still you'll not escape your rival's hand;

That son of Jesse will not cease to fight

Until he seize your crown, your throne, your land.<sup>[2](#_2)</sup>

And tell me, now, where is your sword, your bow,

Your robe which showed your place above the ranks?<sup>[3](#_3)</sup>

Not traded for a death blow from the foe—

**M**uch worse: you swapped them for a shepherd's thanks.

Ungodly gift,<sup>[4](#_4)</sup> a man who pulls down shame

On all our tribe—my son!  Oh what a fall

Is yours. You could have been a king, a name

To long endure. You could have had it all:

Kings to crawl and grovel to your might,

<p><router-link to="/red-4">Zither, myrrh, red wine—ev'ry delight.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        Saul addresses Jonathan.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See I Samuel 20:30-31.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        See I Samuel 18:4.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        The name Jonathan means "God has given."
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="red" to="/red-2">Previous Poem</router-link>
  <router-link class="red" to="/red-4">Next Poem</router-link>
</div>

<br>

<AmazonLink />