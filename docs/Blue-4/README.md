---
title: God’s Blue Throne
meta:
    - name: description
      content: Triple Crown - God’s Blue Throne
    - name: keywords
      content: Triple Crown - God’s Blue Throne
---

# God’s Blue Throne

![](./media/image5.png)

Deign these crowns,<sup>[1](#_15)</sup> too, be worthy in Your sight,

These little crowns of ringlets yet unshorn,<sup>[2](#_2)</sup>

The foolishness of age,<sup>[3](#_3)</sup> and its delight,<sup>[4](#_4)</sup>

The love of children's children yet unborn.

They too may weave their crowns of praise in verse,

Amid life's multiplicity know One\...<sup>[5](#_5)</sup>

Or—O forbid!—soul-sicken, hate and curse

**T**he day that first was heard "Behold your son!"<sup>[6](#_6)</sup>

Each generation weaves its crown anew.

Each generation adds its twisted thread,

Or sets upon the crown a jewel of blue

Amid the strands of gold and brass and lead.

Inured to the despair<sup>[7](#_7)</sup> that life would teach,

<p><router-link to="/blue-5">Safeguard them nigh, and me, within Your reach.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        This is an allusion to John Donne's sonnet crown *La Corona*. Its
        opening line is "Deign at my hands this crown of prayer and
        praise..."
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        Among Chassidim it is customary not to cut a boy's hair until he
        is three years old. The first haircut is given religious
        significance in a ceremony called <i>upsharnish</i>.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        Grandparents lose their decorum around their young grandchildren.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        See Proverbs 17:6.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        See Zechariah 14:9.
    </p>
</div>

<div class="footnote">
    <p id="_6">
        <sup>6</sup>: 
        See Genesis 29:32.
    </p>
</div>

<div class="footnote">
    <p id="_7">
        <sup>7</sup>: 
        See Søren Kierkegaard, <i>The Sickness Unto Death</i>, concerning
        despair.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="blue" to="/blue-3">Previous Poem</router-link>
  <router-link class="blue" to="/blue-5">Next Poem</router-link>
</div>

<br>

<AmazonLink />