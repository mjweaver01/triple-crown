---
title: Dust—All’s Yellow
meta:
    - name: description
      content: Triple Crown - Dust—All’s Yellow
    - name: keywords
      content: Triple Crown - Dust—All’s Yellow
---

# Dust—All’s Yellow

![](./media/image6.png)

Safeguard them nigh, and me, within your reach—

A child's prayer before he goes to bed,

The twice-chewed cud of empty prattle speech

Before his throat is clutched in claws of dread.

Oh, yes, the world has laws by which it\'s run

In mulish bondage to its master's plan.

But justice! Where is justice? There is none!<sup>[1](#_1)</sup>

**C**ondemned to live in vain and die is man,

For he alone has knowledge of his fate,

And will to will the world as it is not.

For every day's tomorrow must he wait,

And every yesterday relive in thought.

And me, O God, whose death will come too late,

<p><router-link to="/yellow-10">You may destroy, completely extirpate.</router-link></p>

### Footnote

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        See Job 6-7. Job responds to Eliphaz, saying that while the
        world may be orderly, it lacks justice.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="yellow" to="/yellow-8">Previous Poem</router-link>
  <router-link class="yellow" to="/yellow-10">Next Poem</router-link>
</div>

<br>

<AmazonLink />