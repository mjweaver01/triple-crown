---
title: Dust—All’s Yellow
meta:
    - name: description
      content: Triple Crown - Dust—All’s Yellow
    - name: keywords
      content: Triple Crown - Dust—All’s Yellow
---

# Dust—All’s Yellow

![](./media/image31.png)

Desire itself no longer within reach,<sup>[1](#_1)</sup>

I still recall when I could feel its grip,

When any wanted thing, mere whispered speech

Would place into my hand, nor would it slip

From out my grasp.<sup>[2](#_2)</sup>  And more:  I was content!

I did not know the maggot gnaw of greed,

But knew the joy of gold and time well spent.

**I** knew the gratitude of those in need

Of generosity with wealth to match.<sup>[3](#_3)</sup>

Now gone, all gone!  All felled within an hour!

The forces of misfortune thus could snatch

My slow accreted honor, wealth, and pow'r.

The brightness of my day enshrouds my night.

<p><router-link to="/yellow-3">Unstinting and severe, the hands that smite.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        The inability to anticipate pleasure is a hallmark of
        depression.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See Job 1:1-3.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        See Job 29:12-16.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="yellow" to="/yellow-1">Previous Poem</router-link>
  <router-link class="yellow" to="/yellow-3">Next Poem</router-link>
</div>

<br>

<AmazonLink />