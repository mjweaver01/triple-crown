---
title: Oh Zealots So Red
meta:
    - name: description
      content: Triple Crown - Oh Zealots So Red
    - name: keywords
      content: Triple Crown - Oh Zealots So Red
---

# Oh Zealots So Red

![](./media/image29.png)

Delay no moment longer.  Take me now

To see the men at arms who guard the land...<sup>[1](#_1)</sup>

You soldiers know what threats we face, and how

To fight.  Now shout:  United! Arms in hand!

Our enemies would cut our land in two,

Would spill our babies' blood, and their war cry

Would rend the air.  But this they will not do!

**L**et us shout:  United! Arms held high!

United shall we pacify our land

From Dan unto Beersheba,<sup>[2](#_2)</sup> and our reach

Extend from our great river to the strand

Where pummel salty waves upon the beach.<sup>[3](#_3)</sup>

One God!  One land!  One people without breach,

<p><router-link to="/red-2">Oh let us be!  United!  Arms outreach!</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        Solomon addresses his army.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See I Kings 5:5.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        See Joshua 1:4.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="red" to="/red-14">Previous Poem</router-link>
  <router-link class="yellow" to="/yellow-1">Next Poem</router-link>
</div>

<br>

<AmazonLink />