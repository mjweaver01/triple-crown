---
title: Oh Zealots So Red
meta:
    - name: description
      content: Triple Crown - Oh Zealots So Red
    - name: keywords
      content: Triple Crown - Oh Zealots So Red
---

# Oh Zealots So Red

![](./media/image24.png)

So cold a night, and none to warm my bed<sup>[1](#_1)</sup>

But you, a youthful stranger hired to spend

The night, to hear my stories dear and dread,

And nod and wonder too when will it end.<sup>[2](#_2)</sup>

I killed a giant once,<sup>[3](#_3)</sup> six cubits high

Or more,<sup>[4](#_4)</sup> cut off his bloody head and raised

It by the hair.<sup>[5](#_5)</sup>  But surely that's not why—

**O** Absalom!  God's anger also blazed

Against the baby.<sup>[6](#_6)</sup>  Better *I* should die.

O Absalom!   I'll go to him; he will 

Not come to me.<sup>[7](#_7)</sup>  Michal, would you deny

A king?<sup>[8](#_8)</sup>  Oh, Jonathan, my escort still?<sup>[9](#_9)</sup>

I see you gathered, all, through murky haze.

<p><router-link to="/red-11">So long a night, I scarce recall the days.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        David addresses Abishag the Shunammite.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See I Kings 1:1-4.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        David's mind begins to drift as he lay dying.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        See I Samuel 17:4.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        See I Samuel 17:51.
    </p>
</div>

<div class="footnote">
    <p id="_6">
        <sup>6</sup>: 
        See II Samuel 12:18.
    </p>
</div>

<div class="footnote">
    <p id="_7">
        <sup>7</sup>: 
        See II Samuel 12:23.
    </p>
</div>

<div class="footnote">
    <p id="_8">
        <sup>8</sup>: 
        David experiences terminal hallucinations. See II Samuel 6:20.
    </p>
</div>

<div class="footnote">
    <p id="_9">
        <sup>9</sup>: 
        See I Samuel 20:41-42.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="red" to="/red-9">Previous Poem</router-link>
  <router-link class="red" to="/red-11">Next Poem</router-link>
</div>

<br>

<AmazonLink />