---
title: God’s Blue Throne
meta:
    - name: description
      content: Triple Crown - God’s Blue Throne
    - name: keywords
      content: Triple Crown - God’s Blue Throne
---

# God’s Blue Throne

![](./media/image4.png)

O God, whom angels praise by day and night,<sup>[1](#_1)</sup>

A poor *paytan*<sup>[2](#_2)</sup> approaches Your blue throne<sup>[3](#_3)</sup>

To offer up before Your grandeur's height

These woven words,<sup>[4](#_4)</sup> these words of worth unknown.

O King, who was, who is, who e'er will be,

You gave three crowns,<sup>[5](#_5)</sup> a gift forevermore:

First priesthood's crown to Aaron's progeny,

**I**n purity, forgiveness to implore,

Then crown of Torah gave to rectify

Your people, and the nations if they heed.

Though now in dust the kingship crown may lie

It yet will gleam on David and his seed.

Now humbly I Your lofty gifts requite;

<p><router-link to="/blue-4">Deign these crowns, too, be worthy in Your sight.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        See Babylonian Talmud (hereinafter referred to as Talmud Bavli)
        <i>Chullin</i> 91b. "And the angels do not sing above until Israel speaks
        below." Also see Isaiah 6:3 and Deuteronomy 6:7.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        A <i>paytan</i> is a person who writes <i>piyyutim</i> (singular,
        <i>piyyut</i>). A <i>piyyut</i> is a Jewish liturgical poem, usually based on
        a poetic scheme such as an acrostic.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        See Ezekiel 1:26.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        This is an allusion to <i>Anim Zemirot</i>, a <i>piyyut</i> attributed to
        the 12th century <i>paytan</i> Rav Yehudah HaChassid. Its opening lines
        are "I shall compose pleasant psalms, and weave together hymns."
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        See <i>Pirkei Avot</i> 4:17. "Rabbi Shimon said, 'There are three
        crowns—the crown of Torah, the crown of priesthood, and the crown
        of kingship...'"
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="blue" to="/blue-2">Previous Poem</router-link>
  <router-link class="blue" to="/blue-4">Next Poem</router-link>
</div>

<br>

<AmazonLink />