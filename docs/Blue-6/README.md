---
title: God’s Blue Throne
meta:
    - name: description
      content: Triple Crown - God’s Blue Throne
    - name: keywords
      content: Triple Crown - God’s Blue Throne
---

# God’s Blue Throne

![](./media/image7.png)

Before the force of nature was unleashed

By Him whose nature knows of no before,<sup>[1](#_1)</sup>

He knew creation's purpose,<sup>[2](#_2)</sup> not yet reached,

A premonition then and evermore:

The sabbath rest, a foretaste of rebirth.<sup>[3](#_3)</sup>

He shaped the male and female and their need,

The animals and plants. He made the earth

**I**n plenitude, for sustenance and seed.

The sun and moon and stars He made from naught,

And supernovae's ancient brilliant blast,

That heavy elements might there be wrought.<sup>[4](#_4)</sup>

Timeless, He is and sees the first and last:

That two small flames of blue she might ignite,<sup>[5](#_5)</sup>

<p><router-link to="/blue-7">"Let there be light," He said—and there was light.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        See Isaiah 44:6.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See Genesis 2:1-3. The Sabbath is the culmination of Creation,
        the only day which God both blessed and sanctified.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        See Talmud Bavli <i>Berachos</i> 57b, which cites the Sabbath as a
        semblance of the world to come.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        After the Big Bang, the material universe was hydrogen and
        helium. Heavier elements were subsequently created in the collapse
        of stars, which we perceive as supernovae.
    </p>
</div>

<div class="footnote">
    <p id="_5">
        <sup>5</sup>: 
        The two small flames are the Sabbath candles.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="blue" to="/blue-5">Previous Poem</router-link>
  <router-link class="blue" to="/blue-7">Next Poem</router-link>
</div>

<br>

<AmazonLink />