---
title: Dust—All’s Yellow
meta:
    - name: description
      content: Triple Crown - Dust—All’s Yellow
    - name: keywords
      content: Triple Crown - Dust—All’s Yellow
---

# Dust—All’s Yellow

![](./media/image33.png)

Such justice, and its judge, do I indict:

Omnipotent, inscrutable, and far

From reason to or reason not to smite

The flesh in which we live or which we are.<sup>[1](#_1)</sup>

My flesh, a charnel feast where flies may spawn,

Precedes me into death.  I sit in dust,

Ooze life, and with a greasy ostracon<sup>[2](#_2)</sup>

**S**crape from my wounds repugnant honey crust.<sup>[3](#_3)</sup>

O flesh so fair, so faithless to your form,

So foul and fetid now, a wounded field,

Repulsive to all life except the worm!

And what avail my prayers?  He is concealed.<sup>[4](#_4)</sup>

No answer comes whence imprecations reach.

<p><router-link to="/yellow-5">The barren walls reverberate a screech.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        Job questions whether there is more to man than his body.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See Job 2:8. An ostracon is synonymous with a potsherd. Ostraca
        were used in Greek society as a tool for ostracizing members from
        society.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        Many skin diseases, and impetigo in particular, are
        characterized by honey-colored scabs fromed from dried serum.
    </p>
</div>

<div class="footnote">
    <p id="_4">
        <sup>4</sup>: 
        See Job 3:22. See also "Eclipse of God," by Martin Buber.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="yellow" to="/yellow-3">Previous Poem</router-link>
  <router-link class="yellow" to="/yellow-5">Next Poem</router-link>
</div>

<br>

<AmazonLink />