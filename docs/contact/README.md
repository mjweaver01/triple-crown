---
title: Contact The Author
pageClass: contact
meta:
    - name: description
      content: Triple Crown - Contact The Author
    - name: keywords
      content: Triple Crown - Contact The Author
---

<div class="contact">
  <h1>Contact the Author</h1>
  <p>Please fill out the form to send a message directly to the author.</p>
  <form id="ct-form" action="//formspree.io/f/echevlen@neomed.edu" method="POST">
    <input id="ct-name" type="text" name="name" placeholder="Name" required=""> 
    <input id="ct-email" type="email" name="_replyto" placeholder="Email" required=""> 
    <input id="ct-subject" name="_subject" type="text" placeholder="Subject" required=""> 
    <textarea id="ct-message" rows="3" placeholder="Leave a Message" name="message" required=""></textarea> 
    <input type="text" name="_gotcha" style="display:none"> 
    <input class="button" type="submit" value="Send">
  </form>
</div>

