---
title: Dust—All’s Yellow
meta:
    - name: description
      content: Triple Crown - Dust—All’s Yellow
    - name: keywords
      content: Triple Crown - Dust—All’s Yellow
---

# Dust—All’s Yellow

![](./media/image32.png)

Unstinting and severe, the hands that smite,

That strangle the newborn with its own cord,

That justify injustice with just might:

I summon now to justice nature's lord.<sup>[1](#_1)</sup>

I witness—I alone am left to tell—

That ten were wrenched like fingers from my hands

And crushed together, buried where they fell,

**E**ntombed beneath the stones and yellow sands.<sup>[2](#_2)</sup>

No daughter\'s corpse was left me to inter,

Nor son to bury me.  Ten children—dead!

Then came my friends to comfort, to confer

Their peace on me.<sup>[3](#_3)</sup>  "God's ways are just" they said,

"He rules the world with justice and with might."

<p><router-link to="/yellow-4">Such justice, and its judge, do I indict.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        See Job 10:1-7.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See Job 1:18-19.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        See Job 1:11-14.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="yellow"to="/yellow-2">Previous Poem</router-link>
  <router-link class="yellow"to="/yellow-4">Next Poem</router-link>
</div>

<br>

<AmazonLink />