---
title: Oh Zealots So Red
meta:
    - name: description
      content: Triple Crown - Oh Zealots So Red
    - name: keywords
      content: Triple Crown - Oh Zealots So Red
---

# Oh Zealots So Red

![](./media/image20.png)

Zither, myrrh, red wine, ev'ry delight

Betray me too, refuse to satisfy,

Assault my hearing, smell, my taste and sight,<sup>[1](#_1)</sup>

As you, in murmurs, plot to nullify

My battered throne. You crave to do me wrong.

Even village girls with dusty feet

Conspire to mock my conquests with their song,<sup>[2](#_2)</sup>

**E**masculate my triumph as defeat.

The blood of my slain thousands flows to sea

To feed their fish-god deep beneath the waves.<sup>[3](#_3)</sup>

How fine an ending that—to cease to be,

To wash the dust from off a soul that craves

No more, of God himself be out of reach.

<p><router-link to="/red-5">Evoked by love, the waves caress the beach.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        In his melancholy, Saul addresses his court. See I Samuel 16:14.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        See I Samuel 18:7.
    </p>
</div>

<div class="footnote">
    <p id="_3">
        <sup>3</sup>: 
        The Philistines worshipped a fish-god named Dagon. See I Samuel
        5:1-5, and Rashi op.cit.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="red" to="/red-3">Previous Poem</router-link>
  <router-link class="red" to="/red-5">Next Poem</router-link>
</div>

<br>

<AmazonLink />