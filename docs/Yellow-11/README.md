---
title: Dust—All’s Yellow
meta:
    - name: description
      content: Triple Crown - Dust—All’s Yellow
    - name: keywords
      content: Triple Crown - Dust—All’s Yellow
---

# Dust—All’s Yellow

![](./media/image37.png)

Erase the yellowed parchment, start anew—

A foolish hope, a vain and futile goal.

I am the sum of all I've suffered through;

Without my scars I'd be another soul.

No, better I had died before my birth,<sup>[1](#_1)</sup>

Or swaddling cloths had been my tiny shroud—

I'd not complain of justice and its dearth.

**E**xistence, to be honest, is not cowed,

But howls the hated truth though none believe.

Myself alone I know, and you do not,

And to unsullied innocence I cleave

In isolation by injustice wrought.<sup>[2](#_2)</sup>

I cannot, till my justice come to me,

<p><router-link to="/yellow-12">Lament some other human tragedy.</router-link></p>

### Footnotes

<div class="footnote">
    <p id="_1">
        <sup>1</sup>: 
        See Job 3:10.
    </p>
</div>

<div class="footnote">
    <p id="_2">
        <sup>2</sup>: 
        Job replies that there is no justice for <i>him</i>.
    </p>
</div>

<br>

<div class="prev-next">
  <router-link class="yellow" to="/yellow-10">Previous Poem</router-link>
  <router-link class="yellow" to="/yellow-12">Next Poem</router-link>
</div>

<br>

<AmazonLink />